/// How many different ways can £2 be made using any number of coins?
pub fn solution() -> usize
{
	// This will only only recurse about 200 times.
	count_ways(200, &[200, 100, 50, 20, 10, 5, 2, 1])
}

/// Calculate the number of ways the specified sum can be met using the given
/// values.
fn count_ways(target: u32, values: &[u32]) -> usize
{
	// Base case
	if values.is_empty() { return 0; }

	let max_possible = target / values[0];
	(0..max_possible + 1).filter_map(|i| {
		match i * values[0] {
			v if v == target => Some(1),
			v if v < target  => Some(count_ways(target - v, &values[1..])),
			_ => None
		}
	}).sum()
}

#[cfg(test)]
mod test
{
	#[test]
	fn project_euler_031()
	{
		assert_eq!(super::solution(), 73682, "Incorrect solution");
	}
}
