extern crate utils;

use utils::LexicographicPerm;

/// Find the sum of all 0 to 9 pandigital numbers whose window-3 parts are the
/// first seven prime numbers.
pub fn solution() -> u64
{
	let mut perms = LexicographicPerm::new((0..10).collect());

	let mut sum = 0;
	while let Some(p) = perms.next() {
		if substring_divisible(p) {
			sum += number(p);
		}
	}

	sum
}

/// Returns true if the number meets the substring divisibility criteria.
fn substring_divisible(perm: &[u8]) -> bool
{
	number(&perm[6..9])    %  2 == 0
	&& number(&perm[5..8]) %  3 == 0
	&& number(&perm[4..7]) %  5 == 0
	&& number(&perm[3..6]) %  7 == 0
	&& number(&perm[2..5]) % 11 == 0
	&& number(&perm[1..4]) % 13 == 0
	&& number(&perm[0..3]) % 17 == 0
}

/// Converts the permutation into a number.
///
/// This function is very slow in debug mode. If that gets annoying, replace
/// this with a `number3` and a `number10` function that just does the raw
/// multiplication and addition.
fn number(perm: &[u8]) -> u64
{
	perm.iter().scan(1, |acc, &i| {let v = *acc * i as u64; *acc *= 10; Some(v)})
	           .fold(0, |acc, i| acc + i)
}


#[cfg(test)]
mod test
{
	#[test]
	fn project_euler_043()
	{
		assert_eq!(super::solution(), 16695334890, "Incorrect solution");
	}

	#[test]
	fn number()
	{
		assert_eq!(super::number(&[1, 2, 3, 5, 4]), 45321);
		assert_eq!(super::number(&[0, 2, 3, 5, 4]), 45320);
		assert_eq!(super::number(&[1, 2, 3, 5, 0]),  5321);
		assert_eq!(super::number(&[1, 2, 0, 5, 4]), 45021);
	}

	#[test]
	fn divisibility()
	{
		let num = [9, 8, 2, 7, 5, 3, 6, 0, 4, 1];
		assert!(super::substring_divisible(&num));
	}
}
