//! Types related to playing games of poker.
//!
//! The bulk of the work here is in the `Hand::type` function. Pretty much
//! everything else is to support parsing from strings.

// This whole file is a bit of a mess. Internally, I'm not particularly proud
// of how things work. Externally, I think I have developed a pretty nifty
// interface.

use std::cmp::Ordering;
use std::str::FromStr;

/// Errors that can happen while parsing a game.
#[derive(Debug)]
pub enum Error
{
	GameLength(usize),
	HandLength(usize),
	CardLength(usize),
	SuitLength(usize),
	SuitType(char),
	ValueLength(usize),
	ValueType(char)
}

/// Result for all poker related errors.
type Result<T> = ::std::result::Result<T, Error>;

/// A two-player game of poker.
#[derive(Debug)]
pub struct Game(pub Hand, pub Hand);

impl FromStr for Game
{
	type Err = Error;

	fn from_str(s: &str) -> Result<Self>
	{
		if s.len() != 29 {
			return Err(Error::GameLength(s.len()));
		}

		let p1 = s[0..14].parse()?;
		let p2 = s[15..29].parse()?;

		Ok(Game(p1, p2))
	}
}

/// Represents a hand in poker.
///
/// Hands has `Ord` implemented in order to compare which hards are better than
/// others.
#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct Hand(pub Card, pub Card, pub Card, pub Card, pub Card);
impl Hand
{
	/// Returns the hand type along with the highest value and the potential
	/// remaining values.
	pub fn hand_type(&self) -> (HandType, [Option<Value>; 5])
	{
		// These are probably already sorted, but do it again just in case.
		let mut cards = [self.0, self.1, self.2, self.3, self.4];
		cards.sort();

		let v = [cards[0].value, cards[1].value, cards[2].value, cards[3].value, cards[4].value];
		let s = [cards[0].suit, cards[1].suit, cards[2].suit, cards[3].suit, cards[4].suit];

		Hand::royal_flush(&v, &s)
		.or_else(|| Hand::straight_flush(&v, &s))
		.or_else(|| Hand::four_of_a_kind(&v))
		.or_else(|| Hand::full_house(&v))
		.or_else(|| Hand::flush(&v, &s))
		.or_else(|| Hand::straight(&v))
		.or_else(|| Hand::three_of_a_kind(&v))
		.or_else(|| Hand::two_pair(&v))
		.or_else(|| Hand::one_pair(&v))
		.unwrap_or_else(|| Hand::high_card(&v))
	}

	/// Returns a value if the cards are a royal flush.
	fn royal_flush(v: &[Value; 5], s: &[Suit; 5]) -> Option<(HandType, [Option<Value>; 5])>
	{
		// A royal flush is just a straight flush with the lowest card being ten.
		if Hand::straight_flush(v, s).is_some() && v[0] == Value::Ten {
			Some((HandType::RoyalFlush, [None; 5]))
		} else { None }
	}

	/// Returns a value if the cards are a straight flush.
	fn straight_flush(v: &[Value; 5], s: &[Suit; 5]) -> Option<(HandType, [Option<Value>; 5])>
	{
		// A straight flush is a hand that is both a straight and a flush
		if Hand::straight(v).and(Hand::flush(v, s)).is_some() {
			let values = [Some(v[0]), None, None, None, None];
			Some((HandType::StraightFlush, values))
		} else { None }
	}

	/// Returns a value if the cards are four of a kind.
	fn four_of_a_kind(v: &[Value; 5]) -> Option<(HandType, [Option<Value>; 5])>
	{
		if v[0..4].windows(2).all(|w| w[0] == w[1]) {
			let values = [Some(v[0]), Some(v[4]), None, None, None];
			Some((HandType::FourOfAKind, values))
		} else if v[1..5].windows(2).all(|w| w[0] == w[1]) {
			let values = [Some(v[1]), Some(v[0]), None, None, None];
			Some((HandType::FourOfAKind, values))
		} else { None }
	}

	/// Returns a value if the cards are a full house.
	fn full_house(v: &[Value; 5]) -> Option<(HandType, [Option<Value>; 5])>
	{
		if v[0] == v[1] && v[1] == v[2] && v[3] == v[4] {
			let values = [Some(v[0]), Some(v[4]), None, None, None];
			Some((HandType::FullHouse, values))
		} else if v[2] == v[3] && v[3] == v[4] && v[0] == v[1] {
			let values = [Some(v[2]), Some(v[0]), None, None, None];
			Some((HandType::FullHouse, values))
		} else { None }
	}

	/// Returns a value if the cards are a flush.
	fn flush(v: &[Value; 5], s: &[Suit; 5]) -> Option<(HandType, [Option<Value>; 5])>
	{
		if s.windows(2).all(|w| w[0] == w[1]) {
			let values = [Some(v[4]), Some(v[3]), Some(v[2]), Some(v[1]), Some(v[0])];
			Some((HandType::Flush, values))
		} else { None }
	}

	/// Returns a value if the cards are a straight.
	fn straight(v: &[Value; 5]) -> Option<(HandType, [Option<Value>; 5])>
	{
		if v.windows(2).all(|w| w[0].next() == Some(w[1])) {
			let values = [Some(v[0]), None, None, None, None];
			Some((HandType::Straight, values))
		} else { None }
	}

	/// Returns a value if the cards are a three of a kind.
	fn three_of_a_kind(v: &[Value; 5]) -> Option<(HandType, [Option<Value>; 5])>
	{
		if v[0] == v[1] && v[1] == v[2] {
			let values = [Some(v[0]), Some(v[4]), Some(v[3]), None, None];
			Some((HandType::ThreeOfAKind, values))
		} else if v[1] == v[2] && v[2] == v[3] {
			let values = [Some(v[1]), Some(v[4]), Some(v[0]), None, None];
			Some((HandType::ThreeOfAKind, values))
		} else if v[2] == v[3] && v[3] == v[4] {
			let values = [Some(v[2]), Some(v[1]), Some(v[0]), None, None];
			Some((HandType::ThreeOfAKind, values))
		} else { None }
	}

	/// Returns a value if the cards are a two pair.
	fn two_pair(v: &[Value; 5]) -> Option<(HandType, [Option<Value>; 5])>
	{
		if v[0] == v[1] && v[2] == v[3] {
			let values = [Some(v[2]), Some(v[1]), Some(v[4]), None, None];
			Some((HandType::TwoPair, values))
		} else if v[0] == v[1] && v[3] == v[4] {
			let values = [Some(v[4]), Some(v[1]), Some(v[2]), None, None];
			Some((HandType::TwoPair, values))
		} else if v[1] == v[2] && v[3] == v[4] {
			let values = [Some(v[4]), Some(v[2]), Some(v[0]), None, None];
			Some((HandType::TwoPair, values))
		} else { None }
	}

	/// Returns a value if the cards are a single pair.
	fn one_pair(v: &[Value; 5]) -> Option<(HandType, [Option<Value>; 5])>
	{
		if v[0] == v[1] {
			let values = [Some(v[1]), Some(v[4]), Some(v[3]), Some(v[2]), None];
			Some((HandType::OnePair, values))
		} else if v[1] == v[2] {
			let values = [Some(v[1]), Some(v[4]), Some(v[3]), Some(v[0]), None];
			Some((HandType::OnePair, values))
		} else if v[2] == v[3] {
			let values = [Some(v[2]), Some(v[4]), Some(v[1]), Some(v[0]), None];
			Some((HandType::OnePair, values))
		} else if v[3] == v[4] {
			let values = [Some(v[4]), Some(v[2]), Some(v[1]), Some(v[0]), None];
			Some((HandType::OnePair, values))
		} else { None }
	}

	/// Returns the high card of the hand.
	fn high_card(v: &[Value; 5]) -> (HandType, [Option<Value>; 5])
	{
		(HandType::HighCard, [Some(v[4]), Some(v[3]), Some(v[2]), Some(v[1]), Some(v[0])])
	}
}

impl PartialOrd for Hand
{
	fn partial_cmp(&self, other: &Self) -> Option<Ordering>
	{
		Some(self.cmp(other))
	}
}

impl Ord for Hand
{
	fn cmp(&self, other: &Self) -> Ordering
	{
		let (p1_type, p1_values) = self.hand_type();
		let (p2_type, p2_values) = other.hand_type();

		p1_type.cmp(&p2_type).then_with(|| p1_values.cmp(&p2_values))
	}
}

impl FromStr for Hand
{
	type Err = Error;

	fn from_str(s: &str) -> Result<Self>
	{
		if s.len() != 14 {
			return Err(Error::HandLength(s.len()));
		}

		Ok(Hand(
			s[0..2].parse()?,
			s[3..5].parse()?,
			s[6..8].parse()?,
			s[9..11].parse()?,
			s[12..14].parse()?,
		))
	}
}

/// The type of hand.
#[derive(PartialEq, Eq, PartialOrd, Ord, Debug)]
pub enum HandType
{
	HighCard,
	OnePair,
	TwoPair,
	ThreeOfAKind,
	Straight,
	Flush,
	FullHouse,
	FourOfAKind,
	StraightFlush,
	RoyalFlush,
}

/// Represents a playing card.
#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Debug)]
pub struct Card
{
	pub value: Value,
	pub suit: Suit,
}

impl FromStr for Card
{
	type Err = Error;

	fn from_str(s: &str) -> Result<Self>
	{
		if s.len() != 2 {
			return Err(Error::CardLength(s.len()));
		}

		Ok(Card {
			value: s[0..1].parse()?,
			suit: s[1..2].parse()?,
		})
	}
}

/// The possible card faces.
#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Debug)]
pub enum Suit
{
	Heart,
	Diamond,
	Club,
	Spade,
}

impl FromStr for Suit
{
	type Err = Error;

	fn from_str(s: &str) -> Result<Self>
	{
		if s.len() != 1 {
			return Err(Error::SuitLength(s.len()));
		}

		Ok(match s.as_bytes()[0] {
			b'H' => Suit::Heart,
			b'D' => Suit::Diamond,
			b'C' => Suit::Club,
			b'S' => Suit::Spade,
			c    => return Err(Error::SuitType(c as char)),
		})
	}
}

/// The value of the card.
#[derive(PartialEq, Eq, PartialOrd, Ord, Copy, Clone, Debug)]
pub enum Value
{
	Two,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace,
}
impl Value
{
	fn next(&self) -> Option<Self>
	{
		match *self {
			Value::Two   => Some(Value::Three),
			Value::Three => Some(Value::Four),
			Value::Four  => Some(Value::Five),
			Value::Five  => Some(Value::Six),
			Value::Six   => Some(Value::Seven),
			Value::Seven => Some(Value::Eight),
			Value::Eight => Some(Value::Nine),
			Value::Nine  => Some(Value::Ten),
			Value::Ten   => Some(Value::Jack),
			Value::Jack  => Some(Value::Queen),
			Value::Queen => Some(Value::King),
			Value::King  => Some(Value::Ace),
			Value::Ace   => None
		}
	}
}

impl FromStr for Value
{
	type Err = Error;

	fn from_str(s: &str) -> Result<Self>
	{
		if s.len() != 1 {
			return Err(Error::ValueLength(s.len()));
		}

		Ok(match s.as_bytes()[0] {
			b'2' => Value::Two,
			b'3' => Value::Three,
			b'4' => Value::Four,
			b'5' => Value::Five,
			b'6' => Value::Six,
			b'7' => Value::Seven,
			b'8' => Value::Eight,
			b'9' => Value::Nine,
			b'T' => Value::Ten,
			b'J' => Value::Jack,
			b'Q' => Value::Queen,
			b'K' => Value::King,
			b'A' => Value::Ace,
			c => return Err(Error::ValueType(c as char)),
		})
	}
}

#[cfg(test)]
mod test
{
	use super::*;

	#[test]
	fn hand()
	{
		let hand: Hand = "5H 5C 6S 7S KD".parse().unwrap();

		assert_eq!(hand, Hand(
			Card { value: Value::Five,  suit: Suit::Heart },
			Card { value: Value::Five,  suit: Suit::Club },
			Card { value: Value::Six,   suit: Suit::Spade },
			Card { value: Value::Seven, suit: Suit::Spade },
			Card { value: Value::King,  suit: Suit::Diamond },
		))
	}

	#[test]
	fn hand_type()
	{
		let high: Hand = "2C 5C 7D 8S QH".parse().unwrap();
		let high_type = HandType::HighCard;
		let high_values = [Some(Value::Queen), Some(Value::Eight), Some(Value::Seven),
		                   Some(Value::Five), Some(Value::Two)];
		assert_eq!(
			high.hand_type(), (high_type, high_values),
			"Highest card Queen"
		);

		let pair: Hand = "2C 3S 8S 8D TD".parse().unwrap();
		let pair_type = HandType::OnePair;
		let pair_values = [Some(Value::Eight), Some(Value::Ten), Some(Value::Three),
		                   Some(Value::Two), None];
		assert_eq!(
			pair.hand_type(), (pair_type, pair_values),
			"Pair of Eights"
		);

		let two_pair: Hand = "3C 3S 8S 8D TD".parse().unwrap();
		let two_pair_type = HandType::TwoPair;
		let two_pair_values = [Some(Value::Eight), Some(Value::Three), Some(Value::Ten),
		                       None, None];
		assert_eq!(
			two_pair.hand_type(), (two_pair_type, two_pair_values),
			"Pairs of Eight and Three"
		);

		let three: Hand = "2D 9C AS AH AC".parse().unwrap();
		let three_type = HandType::ThreeOfAKind;
		let three_values = [Some(Value::Ace), Some(Value::Nine), Some(Value::Two),
		                    None, None];
		assert_eq!(
			three.hand_type(), (three_type, three_values),
			"Three Aces"
		);

		let straight: Hand = "2D 3C 4H 5D 6D".parse().unwrap();
		let straight_type = HandType::Straight;
		let straight_values = [Some(Value::Two), None, None, None, None];
		assert_eq!(
			straight.hand_type(), (straight_type, straight_values),
			"Strait from Two to Six"
		);

		let flush: Hand = "2D 3D 7D 9D QD".parse().unwrap();
		let flush_type = HandType::Flush;
		let flush_values = [Some(Value::Queen), Some(Value::Nine), Some(Value::Seven),
		                    Some(Value::Three), Some(Value::Two)];
		assert_eq!(
			flush.hand_type(), (flush_type, flush_values),
			"Diamon Flush"
		);

		let full_house: Hand = "3C 3D 3S 9S 9D".parse().unwrap();
		let full_house_type = HandType::FullHouse;
		let full_house_values = [Some(Value::Three), Some(Value::Nine),
		                         None, None, None];
		assert_eq!(
			full_house.hand_type(), (full_house_type, full_house_values),
			"Full House"
		);

		let four: Hand = "7C 7D 7S 7H 9D".parse().unwrap();
		let four_type = HandType::FourOfAKind;
		let four_values = [Some(Value::Seven), Some(Value::Nine), None, None, None];
		assert_eq!(
			four.hand_type(), (four_type, four_values),
			"Four Sevens"
		);

		let straight_flush: Hand = "2D 3D 4D 5D 6D".parse().unwrap();
		let straight_flush_type = HandType::StraightFlush;
		let straight_flush_values = [Some(Value::Two), None, None, None, None];
		assert_eq!(
			straight_flush.hand_type(), (straight_flush_type, straight_flush_values),
			"Straight Flush"
		);

		let royal_flush: Hand = "TS JS QS KS AS".parse().unwrap();
		let royal_flush_type = HandType::RoyalFlush;
		let royal_flush_values = [None; 5];
		assert_eq!(
			royal_flush.hand_type(), (royal_flush_type, royal_flush_values),
			"Royal Flush"
		);
	}

	#[test]
	fn game()
	{
		let Game(p1, p2) = "5C AD 5D AC 9C 7C 5H 8D TD KS".parse().unwrap();

		assert_eq!(p1, Hand(
			Card { value: Value::Five, suit: Suit::Club },
			Card { value: Value::Ace,  suit: Suit::Diamond },
			Card { value: Value::Five, suit: Suit::Diamond },
			Card { value: Value::Ace,  suit: Suit::Club },
			Card { value: Value::Nine, suit: Suit::Club },
		));

		assert_eq!(p2, Hand(
			Card { value: Value::Seven, suit: Suit::Club },
			Card { value: Value::Five,  suit: Suit::Heart },
			Card { value: Value::Eight, suit: Suit::Diamond },
			Card { value: Value::Ten,   suit: Suit::Diamond },
			Card { value: Value::King,  suit: Suit::Spade },
		))
	}

	#[test]
	fn winners()
	{
		let Game(p1, p2) = "5H 5C 6S 7S KD 2C 3S 8S 8D TD".parse().unwrap();
		assert!(p1 < p2, "Game 1");

		let Game(p1, p2) = "5D 8C 9S JS AC 2C 5C 7D 8S QH".parse().unwrap();
		assert!(p1 > p2, "Game 2");

		let Game(p1, p2) = "2D 9C AS AH AC 3D 6D 7D TD QD".parse().unwrap();
		assert!(p1 < p2, "Game 3");

		let Game(p1, p2) = "4D 6S 9H QH QC 3D 6D 7H QD QS".parse().unwrap();
		assert!(p1 > p2, "Game 4");

		let Game(p1, p2) = "2H 2D 4C 4D 4S 3C 3D 3S 9S 9D".parse().unwrap();
		assert!(p1 > p2, "Game 5");
	}
}
