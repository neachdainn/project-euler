#[macro_use]
extern crate structopt;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate failure;
extern crate toml;

use std::fs::{File, DirBuilder};
use std::path::Path;
use std::io::{Read, Write};
use failure::ResultExt;
use structopt::StructOpt;

mod answers;

/// Creates a new Rust Crate for the given Project Euler problem.
#[derive(StructOpt, Debug)]
struct Opt
{
	/// The Project Euler problem to be solved.
	number: u32,

	/// The correct answer to the problem.
	answer: Option<i64>,

	/// Do not modify the root Cargo.toml
	#[structopt(short = "t", long ="no-root-toml")]
	root_toml: bool,
}

/// The expected configuration of the root `Cargo.toml`.
#[derive(Deserialize, Serialize, Debug)]
#[serde(deny_unknown_fields)]
struct RootToml
{
	/// The `[profile]` section.
	profile: Option<Profiles>,

	/// The `[workspace]` section of the project config.
	workspace: Workspace,
}

/// The `[profile]` section of the root configuration file.
#[derive(Deserialize, Serialize, Debug)]
struct Profiles
{
	/// The settings for release builds.
	release: Option<Profile>,

	/// The settings for benchmark builds.
	bench: Option<Profile>,
}

/// Profile settings.
#[derive(Deserialize, Serialize, Debug)]
struct Profile
{
	/// Enable link-time optimization.
	lto: Option<bool>,

	/// Number of codegen units to use.
	#[serde(rename="codegen-units")]
	codegen_units: u32,
}

/// The `[workspace]` section of the root configuration file.
#[derive(Deserialize, Serialize, Debug)]
struct Workspace
{
	members: Vec<String>,
}

/// The main logic of the application.
fn run() -> Result<(), failure::Error>
{
	// First, parse the arguments and stuff.
	let opt = Opt::from_args();

	// Read the Cargo.toml that is in the current directory.
	let mut toml = get_cargo_toml().context("Unable to get root Cargo.toml")?;

	// If we're here, we're probably in the right directory. Try to make the
	// subdirectory.
	create_subcrate_directory(&opt).context("Unable to make subscrate directory")?;

	// Now, write the Cargo.toml for the subcrate.
	write_new_cargo_toml(&opt).context("Unable to make subcrate Cargo.toml")?;

	// Then, add the default source file.
	write_new_lib_rs(&opt).context("Unable to create subcrate lib.rs")?;

	// Add the binary file used for profiling.
	write_new_main_rs(&opt).context("Unable to create subcrate main.rs")?;

	// Add the binary file used for benchmarks.
	write_new_bencher_rs(&opt).context("Unable to create subcrate bencher")?;

	// Finally, update the root Cargo.toml and write it out.
	if !opt.root_toml {
		toml.workspace.members.push(format!("{:03}", opt.number));
		update_cargo_toml(toml).context("Unable to update root Cargo.toml")?;
	}

	// Done
	Ok(())
}

/// Reads the root `Cargo.toml`
fn get_cargo_toml() -> Result<RootToml, failure::Error>
{
	let mut f = File::open("Cargo.toml").context("Unable to open file")?;
	let mut contents = String::new();
	f.read_to_string(&mut contents).context("Unable to read file")?;

	Ok(toml::from_str(&contents).context("Unable to parse file")?)
}

/// Creates the subcrate directory.
///
/// This will return an error if the directory already exists or if there is an
/// IO error.
fn create_subcrate_directory(opt: &Opt) -> Result<(), failure::Error>
{
	ensure! {
		!Path::new(&format!("{:03}", opt.number)).exists(),
		"Target directory already exists"
	};

	DirBuilder::new().recursive(true)
	                 .create(format!("{:03}/src", opt.number))
	                 .context("Unable to create problem source directory")?;

	DirBuilder::new().recursive(true)
	                 .create(format!("{:03}/benches", opt.number))
	                 .context("Unable to create problem benches directory")?;
	Ok(())
}

/// Writes the new `Cargo.toml` the subcrate.
fn write_new_cargo_toml(opt: &Opt) -> Result<(), failure::Error>
{
	let mut f = File::create(&format!("{:03}/Cargo.toml", opt.number))
	                  .context("Unable to create new file")?;

	let contents = format!(include_str!("Cargo.toml.template"), opt.number);
	Ok(f.write_all(contents.as_bytes()).context("Unable to write to file")?)
}

/// Writes the default `lib.rs` to the subcrate.
fn write_new_lib_rs(opt: &Opt) -> Result<(), failure::Error>
{
	let mut f = File::create(&format!("{:03}/src/lib.rs", opt.number))
	                  .context("Unable to create new file")?;

	// Determine what the answer is. If it is provided by the user, do that,
	// otherwise look it up from the table. If neither of those work, report
	// and error.
	//
	// We report the error so I don't forget that a default was used and wonder
	// why my answer isn't matching the default.
	let ans = opt.answer
	             .or_else(|| answers::look_up(opt.number))
	             .ok_or(format_err!("Unable to look up answer"))?;

	let contents = format!(include_str!("lib.rs.template"), opt.number, ans);
	Ok(f.write_all(contents.as_bytes()).context("Unable to write to file")?)
}

/// Write the default `main.rs` to the subcrate.
fn write_new_main_rs(opt: &Opt) -> Result<(), failure::Error>
{
	let mut f = File::create(&format!("{:03}/src/main.rs", opt.number))
	                  .context("Unable to create new file")?;

	let contents = format!(include_str!("main.rs.template"), opt.number);
	Ok(f.write_all(contents.as_bytes()).context("Unable to write to file")?)
}

/// Write the default bencher to the subcrate.
fn write_new_bencher_rs(opt: &Opt) -> Result<(), failure::Error>
{
	let mut f = File::create(&format!("{:03}/benches/solution.rs", opt.number))
	                  .context("Unable to create new file")?;

	let contents = format!(include_str!("bencher.rs.template"), opt.number);
	Ok(f.write_all(contents.as_bytes()).context("Unable to write to file")?)
}

/// Writes back the root `Cargo.toml`.
fn update_cargo_toml(toml: RootToml) -> Result<(), failure::Error>
{
	let mut f = File::create("Cargo.toml").context("Unable to open file for writing")?;
	let toml = toml::to_string_pretty(&toml).context("Unable to serialize TOML")?;
	Ok(f.write_all(toml.as_bytes()).context("Unable to write to file")?)
}

/// Entry point of the application.
fn main()
{
	if let Err(e) = run() {
		eprintln!("Error: {}", e);
		e.causes().skip(1).for_each(|c| eprintln!("Caused by: {}", c));
		::std::process::exit(1);
	}
}
