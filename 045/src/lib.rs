/// Find the next triangle number after 40755 that is also pentagonal and
/// hexagonal.
pub fn solution() -> u32
{
	// The problem description gives us the starting numbers for each kind. I
	// can't think of a better way to format this solution other than mutable
	// variables.
	let mut tri = 286;
	let mut pen = 166;
	let hex = 144;

	// The hex numbers grow the fastest so we iterate through them.
	for hex in (hex..).map(hex_number) {
		// Advance the other number types to this point.
		tri = (tri..).take_while(|&t| tri_number(t) <= hex).last().unwrap_or(tri);
		pen = (pen..).take_while(|&p| pen_number(p) <= hex).last().unwrap_or(pen);

		if tri_number(tri) == hex && pen_number(pen) == hex {
			return hex;
		}
	}

	unreachable!();
}

/// Calculate the nth hexagonal number.
fn hex_number(n: u32) -> u32
{
	n * (2 * n - 1)
}

/// Calculate the nth pentagonal number.
fn pen_number(n: u32) -> u32
{
	n * ((3 * n - 1) / 2)
}

/// Calculate the nth triangle number.
fn tri_number(n: u32) -> u32
{
	n * ((n + 1) / 2)
}

#[cfg(test)]
mod test
{
	#[test]
	fn project_euler_045()
	{
		assert_eq!(super::solution(), 1533776805, "Incorrect solution");
	}
}
