use std::ops::Mul;
use std::iter;

/// The underlying type of the fractions.
pub type Num = u32;

/// Represents a rational number that can be simplified using the naive method
/// described in the problem.
///
/// The numerator and denominator of this number are both two-digit numbers and
/// the value of this fraction is less than one.
pub struct NaiveFraction
{
	numerator: Num,
	denominator: Num,
}
impl NaiveFraction
{
	/// Creates a new `NaiveFraction` with the given numerator and denominator.
	///
	/// This function will panic if either the numerator and denominator are
	/// not two-digit numbers or if the fraction is not less than one.
	pub fn from_tuple((n, d): (Num, Num)) -> Self
	{
		assert!(n >= 10 && n < 100, "numerator is not a two-digit number");
		assert!(d >= 10 && d < 100, "denominator is not a two-digit number");
		assert!(n < d, "fraction is greater than one");

		NaiveFraction { numerator: n, denominator: d }
	}

	/// Returns true if the fraction is not considered simple.
	///
	/// According to the problem, a fraction is considered simple if both the
	/// numerator and denominator are multiple of ten.
	pub fn is_complex(&self) -> bool
	{
		self.numerator % 10 != 0 || self.denominator % 10 != 0
	}

	/// Simplify this fraction using the method described in the problem.
	///
	/// This will return `None` if the fraction can't be simplified in this way
	/// or if the simplification leads to an incorrect answer.
	pub fn simplify(self) -> Option<Fraction>
	{
		// First, check to make sure they have numbers in common.
		let (a, b) = (self.numerator / 10, self.numerator % 10);
		let (c, d) = (self.denominator / 10, self.denominator % 10);

		// Cancel out any matching digits
		let (n, d) = match (a, b, c, d) {
			(x, n, y, d) if x == y => (n, d),
			(x, n, d, y) if x == y => (n, d),
			(n, x, y, d) if x == y => (n, d),
			(n, x, d, y) if x == y => (n, d),
			_ => return None,
		};

		// Now, check to make sure the simplification was correct.
		if self.numerator * d == n * self.denominator {
			Some(Fraction { numerator: n, denominator: d})
		} else { None }
	}
}

/// Represents a rational number.
///
/// This type has none of the assumptions that the `NaiveFraction` type does.
///
/// Keep in mind that this type is designed with a Project Euler problem in
/// mind and is not a general purpose type. Specifically, multiplying two of
/// these together may result in infinite recursion (see the documentation for
/// this type's implementation of `Mul`).
#[derive(Debug)]
pub struct Fraction
{
	pub numerator: Num,
	pub denominator: Num
}
impl Fraction
{
	/// Reduces this fraction.
	///
	/// This reduction is done using the correct (GCD) way, rather than the
	/// naive way described in the problem.
	pub fn reduce(self) -> Self
	{
		let gcd = gcd(self.numerator, self.denominator);
		Fraction {
			numerator: self.numerator / gcd,
			denominator: self.denominator / gcd,
		}
	}
}
impl Mul for Fraction
{
	type Output = Self;

	/// The multiplication operator `*`.
	///
	/// This function will attempt to avoid reducing the fraction. However, if
	/// an overflow will occur it will attempt to reduce one or both of the
	/// input fractions.
	///
	/// If there is an overflow but both fractions are already reduced, this
	/// will recurse forever (or until stack overflow). So don't do that.
	fn mul(self, rhs: Self) -> Self::Output
	{
		// Try to avoid calculating the GCD if possible
		let n = self.numerator.checked_mul(rhs.numerator);
		let d = self.denominator.checked_mul(rhs.denominator);
		match (n, d) {
			// No overflow happened, don't do the GCD
			(Some(n), Some(d)) => Fraction {
				numerator: n,
				denominator: d
			},

			// Overflow happened, which means we need to do the GCD. Note how
			// we reduce `self` but switch the order of the arguments. By doing
			// it this way, if we overflow again `rhs` will be reduced, meaning
			// we don't have to add logic to check which side has already been
			// reduced.
			//
			// However, this is also the source of the potential infinite
			// recursion.
			_ => Mul::mul(rhs, self.reduce())
		}
	}
}
impl iter::Product for Fraction
{
	fn product<I>(iter: I) -> Self
		where I: iter::Iterator<Item = Self>
	{
		let base = Fraction { numerator: 1, denominator: 1 };
		iter.fold(base, Mul::mul)
	}
}

/// Returns the greatest common denominator of two numbers.
fn gcd(a: Num, b: Num) -> Num
{
	let (mut a, mut b) = (a, b);
	while b != 0 {
		let t = b;
		b = a % b;
		a = t;
	}

	a
}
