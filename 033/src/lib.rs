mod fraction;
use fraction::{Fraction, NaiveFraction, Num};

/// If the product of the four digit cancelling fractions is given in its
/// lowest common terms, find the value of the denominator.
pub fn solution() -> Num
{
	(10..100).flat_map(|d| (10..d).map(move |n| (n, d)))
	         .map(NaiveFraction::from_tuple)
	         .filter(NaiveFraction::is_complex)
	         .filter_map(NaiveFraction::simplify)
	         .product::<Fraction>()
	         .reduce().denominator
}

#[cfg(test)]
mod test
{
	#[test]
	fn project_euler_033()
	{
		assert_eq!(super::solution(), 100, "Incorrect solution");
	}
}
