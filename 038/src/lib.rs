extern crate utils;

use utils::num;

/// What is the largest 1 to 9 pandigital 9-digit number that can be formed as
/// the concatenated product of an integer with `(1, 2, ..., n)` where `n > 1`?
pub fn solution() -> u32
{
	// As with all of these, we need a reasonable upper limit. We know that `n`
	// must be greater than one, so we need to find the number/point `x` such
	// that `2 * len(x) > 9`.
	const D_LIMIT: u32 = 10_000;

	// We also know that if `n > 9` then the concatenated product has to,
	// basically by definition, by longer than nine digits.
	const N_LIMIT: u32 = 9;

	(1..D_LIMIT).flat_map(|d| (2..N_LIMIT).map(move |n| (d, n)))
	            .filter_map(concat_product)
	            .filter(num::is_pandigital)
	            .max().unwrap()
}


/// Return the concatenated product `d` and the numbers `(1, 2, ..., n)`.
fn concat_product((d, n): (u32, u32)) -> Option<u32>
{
	/// Concatenates two numbers.
	fn concat(num: u32, other: u32) -> Option<u32>
	{
		let shift = match other {
			1...9         => 10,
			10...99       => 100,
			100...999     => 1000,
			1000...9999   => 10000,
			10000...99999 => 100000,
			_ => unimplemented!(),
		};

		num.checked_mul(shift).and_then(|v| v.checked_add(other))
	}

	(1..n + 1).fold(Some(0), |acc, i| acc.and_then(|v| concat(v, i * d)))
}

#[cfg(test)]
mod test
{
	#[test]
	fn project_euler_038()
	{
		assert_eq!(super::solution(), 932718654, "Incorrect solution");
	}

	#[test]
	fn concat()
	{
		assert_eq!(super::concat_product((192, 3)), Some(192384576));
	}
}
