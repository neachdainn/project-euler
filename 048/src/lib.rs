extern crate utils;

use utils::num;

/// Find the last ten digits of the series, `1^1 + 2^2 + 3^3 + ... + 1000^1000`.
pub fn solution() -> u64
{
	let ten_digits = 10000000000;
	(1..1001u64).map(|n| num::modpow_safe(n, n, ten_digits))
	         .sum::<u64>() % ten_digits
}

#[cfg(test)]
mod test
{
	#[test]
	fn project_euler_048()
	{
		assert_eq!(super::solution(), 9110846700, "Incorrect solution");
	}
}
