extern crate utils;

use utils::primes::SieveOfEratosthenes as SoE;
use utils::LexicographicPerm;

/// What is the largest n-digit pandigital prime that exists?
pub fn solution() -> u32
{
	// We know a number is divisible by three if the sum of its digits are
	// divisible by three. This means that the only possible values of `n` are
	// four and seven. We're talking a bet here that we'll find a prime within
	// the `n=7` permutations so we aren't checking against `n=4`.
	let mut perms = LexicographicPerm::new((1..7+1).collect());

	// Now, we're only testing a small number of numbers (`7! = 5040`) for
	// primality. What we *should* do is use a primality test (probabilistic or
	// not) for those numbers. However, generating the sieve for a seven digit
	// number is fairly quick.
	let primes = SoE::new(7_654_321);

	// The permutations go in order and I don't particularly want to do the
	// work to implement a reverse iterator.
	let mut largest = 0u32;
	while let Some(p) = perms.next() {
		let num = convert(p);
		if primes.is_prime(num) {
			largest = num;
		}
	}

	largest
}

/// Coverts the digits to an integer.
fn convert(digits: &[u32]) -> u32
{
	digits.iter().scan(1, |acc, d| {
		let v = *acc * d;
		*acc *= 10;

		Some(v)
	}).sum()
}

#[cfg(test)]
mod test
{
	#[test]
	fn project_euler_041()
	{
		assert_eq!(super::solution(), 7652413, "Incorrect solution");
	}

	#[test]
	fn convert()
	{
		assert_eq!(super::convert(&[1, 2, 3, 4, 5, 6]), 654321);
	}
}
