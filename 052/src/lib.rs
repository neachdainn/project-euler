//! Permuted multiples
//!
//! # Problem Description.
//!
//! It can be seen that the number, 125874, and its double, 251748, contain
//! exactly the same digits, but in a different order.
//!
//! Find the smallest positive integer, *x*, such that 2<i>x</i>, 3<i>x</i>,
//! 4<i>x</i>, 5<i>x</i>, and 6<i>x</i>, contain the same digits.

extern crate utils;
use utils::num::Digits;

/// The type of the answer.
type AnswerType = u32;

/// The answer to the problem.
pub const ANSWER: AnswerType = 142857;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_052::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	// Use mutable vectors instead of iterators to prevent reallocation for
	// every number.
	let mut current = Vec::new();
	let mut next = Vec::new();

	// We only need to search the first sixth of each decade. Any more than
	// that and the 6x number will have more digits than the original
	// number.
	for decade in (1..).scan(1, |acc, _| { *acc *= 10; Some(*acc)}) {
		'outer: for x in decade..((decade * 10) / 6) + 1 {
			// Update the current vector with the new number.
			current.clear();
			current.extend(x.into_digits(10));

			current.sort();

			for y in (2..7).map(|y| y * x) {
				// Update the next vector.
				next.clear();
				next.extend(y.into_digits(10));

				next.sort();

				if next != current {
					continue 'outer;
				}
			}

			return x;
		}
	}

	unreachable!();
}
