//! Odd period square roots
//!
//! # Problem Description.
//!
//! All square roots are periodic when written as continued fractions.
//!
//! The first ten continued fraction representations of (irrational) square
//! roots are:
//!
//! * √2=[1;(2)], period=1
//! * √3=[1;(1,2)], period=2
//! * √5=[2;(4)], period=1
//! * √6=[2;(2,4)], period=2
//! * √7=[2;(1,1,1,4)], period=4
//! * √8=[2;(1,4)], period=2
//! * √10=[3;(6)], period=1
//! * √11=[3;(3,6)], period=2
//! * √12= [3;(2,6)], period=2
//! * √13=[3;(1,1,1,1,6)], period=5
//!
//! Exactly four continued fractions, for N ≤ 13, have an odd period.
//!
//! How many continued fractions for N ≤ 10000 have an odd period?


/// The type of the answer.
type AnswerType = usize;

/// The answer to the problem.
pub const ANSWER: AnswerType = 1322;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_064::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	(2..10_001).map(continued_fraction_period)
	           .filter(|&c| c % 2 == 1)
	           .count()
}

/// Counts the perioud of the continued fraction of √N
fn continued_fraction_period(nn: u32) -> usize
{
	// First, make sure this isn't a perfect square.
	let a0 = (nn as f64).sqrt() as u32;
	if a0 * a0 == nn {
		return 0;
	}

	// This algorithm comes directly from Wikipedia
	let mut period = 0;

	let mut m = 0;
	let mut d = 1;
	let mut a = a0;

	loop {
		m = d * a - m;
		d = (nn - m * m) / d;
		a = (a0 + m) / d;

		period += 1;

		if a == 2 * a0 {
			break;
		}
	}

	period
}

#[cfg(test)]
mod test
{
	#[test]
	fn continued_fraction()
	{
		assert_eq!(super::continued_fraction_period(2),  1, "√2");
		assert_eq!(super::continued_fraction_period(3),  2, "√3");
		assert_eq!(super::continued_fraction_period(5),  1, "√5");
		assert_eq!(super::continued_fraction_period(6),  2, "√6");
		assert_eq!(super::continued_fraction_period(7),  4, "√7");
		assert_eq!(super::continued_fraction_period(8),  2, "√8");
		assert_eq!(super::continued_fraction_period(10), 1, "√10");
		assert_eq!(super::continued_fraction_period(11), 2, "√11");
		assert_eq!(super::continued_fraction_period(12), 2, "√12");
		assert_eq!(super::continued_fraction_period(13), 5, "√13");
	}
}
