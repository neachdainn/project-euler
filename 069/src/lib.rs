//! Totient maximum
//!
//! # Problem Description.
//!
//! Euler's Totient function, φ(n) [sometimes called the phi function], is used
//! to determine the number of numbers less than n which are relatively prime
//! to n. For example, as 1, 2, 4, 5, 7, and 8, are all less than nine and
//! relatively prime to nine, φ(9)=6.
//!
//! | n   | Relatively Prime  | φ(n)  |  n/φ(n)   |
//! |-----|-------------------|-------|-----------|
//! |  2  |                1  |    1  |         2 |
//! |  3  |              1,2  |    2  |       1.5 |
//! |  4  |              1,3  |    2  |         2 |
//! |  5  |          1,2,3,4  |    4  |      1.25 |
//! |  6  |              1,5  |    2  |         3 |
//! |  7  |      1,2,3,4,5,6  |    6  | 1.1666... |
//! |  8  |          1,3,5,7  |    4  |         2 |
//! |  9  |      1,2,4,5,7,8  |    6  |       1.5 |
//! | 10  |          1,3,7,9  |    4  |       2.5 |
//!
//! It can be seen that n=6 produces a maximum n/φ(n) for n ≤ 10.
//!
//! Find the value of n ≤ 1,000,000 for which n/φ(n) is a maximum.

extern crate utils;
use utils::primes::Sieve;

/// The type of the answer.
type AnswerType = u32;

/// The answer to the problem.
pub const ANSWER: AnswerType = 510510;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_069::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	// Brute force is effective but very slow for this problem. Instead, we
	// need to come up with a value for n such that n is maximized and φ(n) is
	// minimized. We maximize n by creating a number with as many prime factors
	// as possible and we minimize φ(n) by making sure those prime factors are
	// both unique and small.
	//
	// So, this really boils down to 2·3·5·7·11·17 = 510,510. We know this,
	// because 510,510·19 > 1,000,000.
	//
	// I could probably justify making this one a constant, since it was worked
	// out by hand so easily, but eh.
	Sieve::new(100).iter()
		.scan(1, |acc, p| {*acc *= p; Some(*acc)})
		.take_while(|&n| n <= 1_000_000)
		.last()
		.unwrap()
}
