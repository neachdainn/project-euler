//! Lychrel numbers
//!
//! # Problem Description.
//!
//! If we take 47, reverse and add, 47 + 74 = 121, which is palindromic.
//!
//! Not all numbers produce palindromes so quickly. For example,
//!
//! * 349 + 943 = 1292
//! * 1292 + 2921 = 4213
//! * 4213 + 3124 = 7337
//!
//! That is, 349 took three iterations to arrive at a palindrome.
//!
//! Although no one has proved it yet, it is thought that some numbers, like
//! 196, never produce a palindrome. A number that never forms a palindrome
//! through the reverse and add process is called a Lychrel number. Due to the
//! theoretical nature of these numbers, and for the purpose of this problem,
//! we shall assume that a number is Lychrel until proven otherwise. In
//! addition you are given that for every number below ten-thousand, it will
//! either (i) become a palindrome in less than fifty iterations, or, (ii) no
//! one, with all the computing power that exists, has managed so far to map it
//! to a palindrome. In fact, 10677 is the first number to be shown to require
//! over fifty iterations before producing a palindrome:
//! 4668731596684224866951378664 (53 iterations, 28-digits).
//!
//! Surprisingly, there are palindromic numbers that are themselves Lychrel
//! numbers; the first example is 4994.
//!
//! How many Lychrel numbers are there below ten-thousand?

extern crate utils;
use utils::num::{BigUInt, Digits};

/// The type of the answer.
type AnswerType = usize;

/// The answer to the problem.
pub const ANSWER: AnswerType = 249;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_055::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	(0..10_000u32).filter(is_lychrel_number).count()
}

/// Returns true if the number is a Lychrel number.
fn is_lychrel_number(n: &u32) -> bool
{
	// This whole thing was an absolute mess until I realized how big these
	// numbers would get. Lots of swearing about overflowing operations and
	// such. It does make me glad I'm using Rust, though, since I probably
	// would have had a difficult time figuring out what was going wrong
	// without the panicking.
	let mut n = BigUInt::new(*n);

	for _ in 0..50 {
		// Do the reverse sum
		let rev = reverse(&n);
		n += rev;

		// We have to collect the digits, since BigUInt doesn't have a reverse
		// iterator (yet)
		let rev_digits = n.to_digits(10).collect::<Vec<_>>();

		// We only have to check half of the digits to determine if it is
		// pandigital, with the odd one out not counting.
		let check_count = rev_digits.len() / 2;

		// Create and compare the iterators
		let forward = n.to_digits(10).take(check_count);
		let backward = rev_digits.into_iter().rev().take(check_count);

		if forward.eq(backward) {
			return false;
		}
	}

	true
}

/// Reverses the number (base 10)
fn reverse(num: &BigUInt) -> BigUInt
{
	let mut digits: Vec<_> = num.to_digits(10).collect();
	digits.reverse();

	BigUInt::from_digits(digits, 10).unwrap()
}
