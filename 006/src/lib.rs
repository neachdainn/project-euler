//! Sum square difference
//!
//! # Problem Description.
//!
//! The sum of the squares of the first ten natural numbers is
//! 1² + 2² + … + 10² = 385
//!
//! The square of the sum of the first ten natural numbers is
//! (1 + 2 + … + 10)² = 55² = 3025
//!
//! Hence the difference between the sum of the squares of the first ten
//! natural numbers and the square of the sum is 3025 − 385 = 2640.
//!
//! Find the difference between the sum of the squares of the first one hundred
//! natural numbers and the square of the sum.


/// The type of the answer.
type AnswerType = u32;

/// The answer to the problem.
pub const ANSWER: AnswerType = 25164150;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_006::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	let sum_of_squares: u32 = (1..101).map(|x| x*x).sum();
	let square_of_sum = (1..101).sum::<u32>().pow(2);

	square_of_sum - sum_of_squares
}
