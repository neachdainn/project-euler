//! Lattice paths
//!
//! # Problem Description.
//!
//! Starting in the top left corner of a 2×2 grid, and only being able to move
//! to the right and down, there are exactly 6 routes to the bottom right
//! corner.
//!
//! How many such routes are there through a 20×20 grid?

/// The type of the answer.
type AnswerType = i64;

/// The answer to the problem.
pub const ANSWER: AnswerType = 137846528820;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_015::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	// The problem with this one is that it's difficult to turn into a
	// legitimate programming problem because it is easy to analyze away. In a
	// 20x20 grid, there will be a total of 40 moves, 20 of which much be to
	// the right. That means the number of possible paths is equal to (40
	// choose 20)... Which is well defined and equal to 137846528820.
	137846528820

	// Alternatively, this could have been done as a dynamic programming
	// problem.
}
