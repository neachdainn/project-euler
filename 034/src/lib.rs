extern crate utils;

use utils::mem::Memoizer;
use utils::num::Digits;

/// Find the sum of all numbers which are equal to the sum of the factorial of their digits.
pub fn solution() -> u64
{
	// As always, we need to come up with reasonable bounds to search. This is
	// similar to problem 30 in that we need to find the point where the length
	// of the sum of factorials is less than the length of the number. In other
	// words, we need to find `x` such that `len(len(x) * 9!) < len(x)`. This
	// will serve as the upper bound.
	//
	// Simple testing shows that this upper bound is 10,000,000.
	const LIMIT: u64 = 10_000_000;

	let mem = Memoizer::new(|m, a| if a < 2 { 1 } else { a * m.call(a - 1) } );

	(3..LIMIT).map(|d| (d, d.into_digits(10).map(|d| mem.call(d)).sum()))
	          .filter(|&(d, s)| d == s)
	          .map(|(_, s)| s)
	          .sum()
}

#[cfg(test)]
mod test
{
	#[test]
	fn project_euler_034()
	{
		assert_eq!(super::solution(), 40730, "Incorrect solution");
	}
}
