extern crate utils;

use utils::primes::Sieve;

/// Find the first four consecutive integers to have four distinct prime
/// factors each. What is the first of these numbers?
pub fn solution() -> u64
{
	let primes = Sieve::new(1000);

	// Allocate these here so we don't have to recalculate them each time.
	let start = 647;
	let mut f0;
	let mut f1 = primes.factors(start + 1).unwrap();
	let mut f2 = primes.factors(start + 2).unwrap();
	let mut f3 = primes.factors(start + 3).unwrap();

	for n in (start + 4).. {
		f0 = f1;
		f1 = f2;
		f2 = f3;
		f3 = primes.factors(n).unwrap();

		if f0.len() == 4 && f1.len() == 4 && f2.len() == 4 && f3.len() == 4 {
			return n - 3;
		}
	}

	unreachable!();
}

#[cfg(test)]
mod test
{
	#[test]
	fn project_euler_047()
	{
		assert_eq!(super::solution(), 134043, "Incorrect solution");
	}
}
