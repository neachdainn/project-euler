//! Totient permutation
//!
//! # Problem Description.
//!
//! Euler's Totient function, φ(n) [sometimes called the phi function], is used
//! to determine the number of positive numbers less than or equal to n which
//! are relatively prime to n. For example, as 1, 2, 4, 5, 7, and 8, are all
//! less than nine and relatively prime to nine, φ(9)=6.
//!
//! The number 1 is considered to be relatively prime to every positive number,
//! so φ(1)=1.
//!
//! Interestingly, φ(87109)=79180, and it can be seen that 87109 is a
//! permutation of 79180.
//!
//! Find the value of n, 1 < n < 10⁷, for which φ(n) is a permutation of n and
//! the ratio n/φ(n) produces a minimum.

extern crate utils;
use utils::{primes::Sieve, num::Digits};

/// The type of the answer.
type AnswerType = u32;

/// The answer to the problem.
pub const ANSWER: AnswerType = 8319823;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_070::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	// We are trying to minimize the ration φ(n)/n, which means we need a large
	// number with as few prime factors as possible. Since φ(p) = p - 1 if p is
	// prime, that means that we have to have at least two prime factors. The
	// square root of 10⁷ is approximately 3162 so we need to search for
	// products of two primes near that number.
	//
	// I have no proof that the best solution only uses two primes. However, my
	// answer does agree with the ones I have seen online and it makes sense
	// for the ratio to get worse the more primes there are.
	let (lower, upper) = (2000, 4000);
	let primes = Sieve::new(upper);
	primes.iter()
		.skip_while(|&p0| p0 < lower)
		.flat_map(|p0| {
			primes.iter()
				.skip_while(move |&p1| p1 < p0)
				.map(move |p1| (p0, p1))
		})
		.map(|(p0, p1)| {
			let num = p0 * p1;
			let totient = (p0 - 1) * (p1 - 1);
			(num, totient)
		})
		.filter(|&(n, _)| n <= 10_000_000)
		.filter(|&(n, t)| is_perm(n, t))
		.map(|(n, t)| (n, n as f64 / t as f64))
		.min_by(|(_, a), (_, b)| a.partial_cmp(b).unwrap())
		.map(|(n, _)| n)
		.unwrap()
}

/// Returns true if the provided numbers are permutations of each other.
fn is_perm(a: AnswerType, b: AnswerType) -> bool
{
	let mut counts = [0isize; 10];

	a.to_digits(10).for_each(|d| counts[d as usize] += 1);
	b.to_digits(10).for_each(|d| counts[d as usize] -= 1);

	counts.iter().all(|&d| d == 0)
}
