extern crate utils;

mod trim;

use utils::num::Digits;
use utils::primes::SieveOfEratosthenes as SoE;
use trim::{Trim, Direction};

/// Find the sum of the only eleven primes that are both trucatable from left
/// to right and right to left.
pub fn solution() -> u32
{
	// Make a guess at the upper limit.
	let primes = SoE::new(1_000_000);

	// Skip the first four since 2, 3, 5, and 7 don't count
	primes.iter().skip(4).filter(|&p| {
		// This may not be the most efficient, but it is the quickest to write.
		let digits = p.into_digits(10).collect::<Vec<_>>();

		Trim::new(&digits, Direction::Left).all(|n| primes.is_prime(n)) &&
		Trim::new(&digits, Direction::Right).all(|n| primes.is_prime(n))
	}).take(11).sum()
}

#[cfg(test)]
mod test
{
	#[test]
	fn project_euler_037()
	{
		assert_eq!(super::solution(), 748317, "Incorrect solution");
	}
}
