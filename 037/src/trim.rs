/// An iterator over the left trimming of the number.
pub struct Trim<'a>
{
	digits: &'a [u32],
	direction: Direction,
}
impl<'a> Trim<'a>
{
	pub fn new(digits: &'a [u32], direction: Direction) -> Self
	{
		Trim { digits, direction }
	}
}
impl<'a> Iterator for Trim<'a>
{
	type Item = u32;

	fn next(&mut self) -> Option<Self::Item>
	{
		// If the digits are empty, we're done
		if self.digits.is_empty() { return None; }

		// Now, calculate the current number.
		let num = self.digits.iter().fold((0, 1), |acc, i| (acc.0 + acc.1 * i, acc.1 * 10)).0;

		// Do the trim.
		self.digits = match self.direction {
			Direction::Left => &self.digits[..self.digits.len() - 1],
			Direction::Right => &self.digits[1..],
		};

		// Return the current number
		Some(num)
	}
}

/// The direction from which the trim should happen.
pub enum Direction
{
	Left,
	Right,
}
