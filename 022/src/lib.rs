pub const NAMES: &'static str = include_str!("names.txt");

/// What is the total of all the name scores in the file?
pub fn solution() -> i32
{
	// This will be slow and a lot of pre-processing could be done to help
	// this, but I suppose that's moot.
	let mut names: Vec<String> = NAMES.split(',')
	                                  .map(|s| s.chars()
	                                            .filter(|&c| c.is_alphabetic())
	                                            .collect())
	                                  .collect();
	names.sort();

	names.iter().enumerate()
	     .map(|(e, s)| ((e + 1) as i32, s))
	     .map(|(e, s)| (e, s.bytes().map(|b| ((b - b'A') + 1) as i32).sum::<i32>()))
	     .map(|(e, s)| e * s)
	     .sum()
}

#[cfg(test)]
mod test
{
	#[test]
	fn project_euler_022()
	{
		assert_eq!(super::solution(), 871198282, "Incorrect solution");
	}
}
