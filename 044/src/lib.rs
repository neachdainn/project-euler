use std::collections::BTreeMap;
use std::cmp;

type BTreeSet<T> = BTreeMap<T, ()>;

/// Find the pair of pentagonal numbers such that their sum and difference are
/// both pentagonal numbers and the difference is minimized.
pub fn solution() -> u32
{
	// I have no mathematical backing for this solution. The entirety of my
	// reasoning is that the pentagonal numbers get further and further apart
	// the larger they are, so if I diagonalize across the pentagonal numbers
	// the first pair that meet the criteria is probably also the minimal pair.
	// The answers I found online all seem to agree with my answer, so I'm not
	// going to question this.
	//
	// Unlike Problem 42, we'd have to collect a large number of pentagonal
	// numbers before we find the solution. Because of that, we can't just put
	// these into an truth array and call it good. We're going to have to place
	// things in some kind of set. We're using a BTreeMap as a set, since we
	// want the items to come out in order.
	let pentagonals: BTreeSet<_> = (1..3000).map(|n| ((n * (3 * n - 1) / 2), ())).collect();

	// Do the diagonalization
	for (i, k) in pentagonals.keys().enumerate() {
		for j in pentagonals.keys().take(i + 1) {
			let diff = cmp::max(k, j) - cmp::min(k, j);
			let sum = k + j;

			if pentagonals.contains_key(&diff) && pentagonals.contains_key(&sum) {
				// Assume the first one is also the minimal
				return diff;
			}
		}
	}

	unreachable!();
}

#[cfg(test)]
mod test
{
	#[test]
	fn project_euler_044()
	{
		assert_eq!(super::solution(), 5482660, "Incorrect solution");
	}
}
