//! Longest Collatz sequence
//!
//! # Problem Description.
//!
//! The following iterative sequence is defined for the set of positive
//! integers:
//!
//! * n → n/2 (n is even)
//! * n → 3n + 1 (n is odd)
//!
//! Using the rule above and starting with 13, we generate the following
//! sequence: 13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
//!
//! It can be seen that this sequence (starting at 13 and finishing at 1)
//! contains 10 terms. Although it has not been proved yet (Collatz Problem),
//! it is thought that all starting numbers finish at 1.
//!
//! Which starting number, under one million, produces the longest chain?
//!
//! NOTE: Once the chain starts the terms are allowed to go above one million.

extern crate utils;
use utils::mem::Memoizer;

/// The type of the answer.
type AnswerType = u64;

/// The answer to the problem.
pub const ANSWER: AnswerType = 837799;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_014::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	let memoizer = Memoizer::new(|m, num| {
		if num == 1 { return 1; }

		let next = if num % 2 == 0 { num / 2 } else { 3 * num + 1 };
		1 + m.call(next)
	});

	(1..1_000_000).map(|d| (d, memoizer.call(d)))
	              .max_by_key(|&(_, l)| l)
	              .map(|(d, _)| d)
	              .unwrap()
}
