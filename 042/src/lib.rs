mod words;
use words::WORDS;

/// How many of the given words are triangle words?
pub fn solution() -> usize
{
	// Calculate an array that specifies if a number is a triangle number
	let triangle_numbers = {
		// Find the size of the longest word in the array.
		let longest = WORDS.iter().map(|w| w.len()).max().unwrap();
		let largest_possible_number = longest * 26;

		// Now, we could make a set of all triangle numbers up to that value, but
		// having an array of that size isn't too huge and will be a faster lookup.
		let mut triangle_numbers = vec![false; largest_possible_number];
		(1..).map(|n| (n * (n + 1)) / 2)
			 .take_while(|&n| n < largest_possible_number)
			 .for_each(|n| triangle_numbers[n] = true);
		triangle_numbers
	};

	// Now calculate which words are triangle words and count them.
	WORDS.iter()
	     .map(|&w| w.bytes()
	                .map(|c| (c - b'A' + 1) as usize)
	                .sum::<usize>())
	     .filter(|&n| triangle_numbers[n])
	     .count()
}

#[cfg(test)]
mod test
{
	#[test]
	fn project_euler_042()
	{
		assert_eq!(super::solution(), 162, "Incorrect solution");
	}
}
