//! Magic 5-gon ring
//!
//! # Problem Description.
//!
//! Consider the following "magic" 3-gon ring, filled with the numbers 1 to 6,
//! and each line adding to nine.
//!
//! ![Magic 3-gon ring](https://projecteuler.net/project/images/p068_1.gif)
//!
//! Working **clockwise**, and starting from the group of three with the
//! numerically lowest external node (4,3,2 in this example), each solution can
//! be described uniquely. For example, the above solution can be described by
//! the set: 4,3,2; 6,2,1; 5,1,3.
//!
//! It is possible to complete the ring with four different totals: 9, 10, 11,
//! and 12. There are eight solutions in total.
//!
//! | Total |    Solution Set     |
//! |-------|---------------------|
//! |     9 | 4,2,3; 5,3,1; 6,1,2 |
//! |     9 | 4,3,2; 6,2,1; 5,1,3 |
//! |    10 | 2,3,5; 4,5,1; 6,1,3 |
//! |    10 | 2,5,3; 6,3,1; 4,1,5 |
//! |    11 | 1,4,6; 3,6,2; 5,2,4 |
//! |    11 | 1,6,4; 5,4,2; 3,2,6 |
//! |    12 | 1,5,6; 2,6,4; 3,4,5 |
//! |    12 | 1,6,5; 3,5,4; 2,4,6 |
//!
//! By concatenating each group it is possible to form 9-digit strings; the
//! maximum string for a 3-gon ring is 432621513.
//!
//! Using the numbers 1 to 10, and depending on arrangements, it is possible to
//! form 16- and 17-digit strings. What is the maximum **16-digit** string for
//! a "magic" 5-gon ring?
//!
//! ![Magic 5-gon ring](https://projecteuler.net/project/images/p068_2.gif)


extern crate utils;
use utils::LexicographicPerm;

/// The type of the answer.
type AnswerType = i64;

/// The answer to the problem.
pub const ANSWER: AnswerType = 6531031914842725;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_068::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	// This is possible to solve by hand but unlike Problem 15 I can't really
	// justify to myself having this just be the hand-done solution. As such,
	// this solution is kind of half way between the hand method and the brute
	// force.
	//
	// We're trying to maximize the number we get, so we know that the outer
	// ring will be composed of the numbers 6-10. We also know that we sort the
	// edges by the minimum, so 6 always needs to be the first value.
	//
	// We can use this information to split the permutations into three groups:
	// 1. 6
	// 2. [7, 8, 9, 10]
	// 3. [1, 2, 3, 4, 5]
	// This will be significantly faster than doing the full permutation.
	let mut outer_perms = LexicographicPerm::new(vec![7, 8, 9, 10]);
	let mut inner_ring_backing = vec![1, 2, 3, 4, 5];

	let mut max = 0;
	while let Some(outer) = outer_perms.next() {
		let mut inner_perms = LexicographicPerm::new(inner_ring_backing);
		while let Some(inner) = inner_perms.next() {
			// First, convert the permutations into a full ring to make life easier.
			let ring = [6, outer[0], outer[1], outer[2], outer[3],
			            inner[0], inner[1], inner[2], inner[3], inner[4]];

			// Now, check to make sure it's a valid ring and, if it is, see if
			// it's the largest we've seen.
			if is_valid_ring(&ring) {
				// We don't have to check to see if we have a 17 digit number
				// since we've guaranteed that 10 will be in an outer node.
				max = ::std::cmp::max(max, ring_to_num(&ring));
			}
		}

		// Reclaim the backing vector so we don't have to allocate again.
		inner_ring_backing = inner_perms.take();
	}

	max
}

/// Checks to see if the given permutation is a valid Magic 5-gon ring.
///
/// The order is the first five are the outer rings going clockwise followed by
/// the inner ring going clockwise.
fn is_valid_ring(ngon: &[u8; 10]) -> bool
{
	let v = ngon[0] + ngon[5] + ngon[6];

	ngon[1] + ngon[6] + ngon[7] == v
	&& ngon[2] + ngon[7] + ngon[8] == v
	&& ngon[3] + ngon[8] + ngon[9] == v
	&& ngon[4] + ngon[9] + ngon[5] == v
}

/// Returns the Magic 5-gon ring as a number.
///
/// Order is the same as the one in `is_valid_ring`.
fn ring_to_num(ngon: &[u8]) -> i64
{
	// This could be done without the new array but stack arrays are cheap and
	// this is easier to mentally process.
	let lines = [ngon[0], ngon[5], ngon[6],
	             ngon[1], ngon[6], ngon[7],
	             ngon[2], ngon[7], ngon[8],
	             ngon[3], ngon[8], ngon[9],
	             ngon[4], ngon[9], ngon[5]];

	let mut res = 0;
	for &n in lines.iter() {
		res *= if n == 10 { 100i64 } else { 10i64 };
		res += n as i64;
	}

	res
}

#[cfg(test)]
mod test
{
	#[test]
	fn ring_to_num()
	{
		assert_eq!(
			super::ring_to_num(&[6, 7, 8, 9, 10, 1, 2, 3, 4, 5]),
			612_723_834_945_1051,
			"ring not parsed into number correctly"
		);
	}
}
