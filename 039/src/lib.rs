extern crate utils;

use utils::PythagoreanTriples;

/// The maximum allowed triangle perimeter.
const MAX_PERIMETER: u32 = 1000;

/// For which value of `p ≤ 1000` is the number of solutions maximised?
pub fn solution() -> u32
{
	let mut count = [0; (MAX_PERIMETER + 1) as usize];

	// Converts the primitive triples into all triples
	PythagoreanTriples::new().take_while(in_range)
	                         .flat_map(|(a, b, c)| (1..).map(move |m| (m * a, m * b, m * c))
	                                                    .take_while(in_range))
	                         .map(|(a, b, c)| a + b + c)
	                         .for_each(|v| count[v as usize] += 1);

	count.iter().enumerate().max_by_key(|&(_, c)| c).map(|(e, _)| e as u32).unwrap()
}

/// Returns true if the given triangle is within the allowed size.
fn in_range(t: &(u32, u32, u32)) -> bool
{
	t.0 + t.1 + t.2 <= MAX_PERIMETER
}

#[cfg(test)]
mod test
{
	#[test]
	fn project_euler_039()
	{
		assert_eq!(super::solution(), 840, "Incorrect solution");
	}
}
