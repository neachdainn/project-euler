extern crate utils;
use utils::fib::FibonacciSequence;
use utils::num::{BigUInt, Digits};

/// What is the index of the first term in the Fibonacci sequence to contain
/// 1000 digits?
pub fn solution() -> usize
{
	// Project Euler indexes start at one
	FibonacciSequence::new().map(|f: BigUInt| f.into_digits(10).count()).position(|l| l >= 1000).unwrap() + 1
}

#[cfg(test)]
mod test
{
	#[test]
	fn project_euler_025()
	{
		assert_eq!(super::solution(), 4782, "Incorrect solution");
	}
}
