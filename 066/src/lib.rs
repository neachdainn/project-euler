//! Diophantine equation
//!
//! # Problem Description.
//!
//! Consider quadratic Diophantine equations of the form: x² – Dy² = 1
//!
//! For example, when D=13, the minimal solution in x is 649² – 13×180² = 1.
//!
//! It can be assumed that there are no solutions in positive integers when D
//! is square.
//!
//! By finding minimal solutions in x for D = {2, 3, 5, 6, 7}, we obtain the
//! following:
//!
//! * 3² – 2×2² = 1
//! * 2² – 3×1² = 1
//! * 9² – 5×4² = 1
//! * 5² – 6×2² = 1
//! * 8² – 7×3² = 1
//!
//! Hence, by considering minimal solutions in x for D ≤ 7, the largest x is
//! obtained when D=5.
//!
//! Find the value of D ≤ 1000 in minimal solutions of x for which the largest
//! value of x is obtained.

extern crate utils;
use utils::num::BigInt;

/// The type of the answer.
type AnswerType = i32;

/// The answer to the problem.
pub const ANSWER: AnswerType = 661;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_066::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	(2..1001).filter(|&dd| { let d = (dd as f64).sqrt() as i32; d * d != dd })
	         .map(|d| (d, solve_pell(d)))
	         .max_by(|&(_, ref x), &(_, ref y)| x.cmp(y))
	         .map(|(d, _)| d)
	         .unwrap()
}

/// Solves the Pell equation using the Chakravala method.
///
/// I really don't understand it but I was at least able to implement it.
fn solve_pell(dd: i32) -> BigInt
{
	let (mut x, mut y, mut k) = (BigInt::new(1), BigInt::new(0), 1);
	let mut p = 1;
	let d = (dd as f64).sqrt();

	// Loop as long as we have either a trivial solution or the solution to the
	// wrong equation.
	while k != 1 || y.is_zero() {
		// Make sure that (x + p*y) is divisible by k and the (m² - N) is
		// minimal
		p = k * (p / k + 1) - p;
		p = p - ((p as f64 - d) / k as f64) as i32 * k;

		/*let x_p = x;
		x = (p * x_p + dd * y) / k.abs();
		y = (p * y + x_p) / k.abs();*/

		let y_p = y.clone();
		y *= p;
		y += &x;
		y /= k.abs();

		x *= p;
		x += dd * y_p;
		x /= k.abs();

		k = (p * p - dd) / k;
	}

	println!("{}² - {} · {}² = {}", x.clone(), dd, y.clone(), (x.clone() * x.clone()) - dd * (y.clone() * y.clone()));
	x
}
