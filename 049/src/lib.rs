//! Prime Permutations
//!
//! # Problem Description
//!
//! The arithmetic sequence, 1487, 4817, 8147, in which each of the terms
//! increases by 3330, is unusual in two ways: (i) each of the three terms are
//! prime, and, (ii) each of the 4-digit numbers are permutations of one
//! another.
//!
//! There are no arithmetic sequences made up of three 1-, 2-, or 3-digit
//! primes, exhibiting this property, but there is one other 4-digit increasing
//! sequence.
//!
//! What 12-digit number do you form by concatenating the three terms in this
//! sequence?

extern crate utils;
use utils::primes::Sieve;

/// The type of the answer.
type AnswerType = u64;

/// The answer to the problem.
pub const ANSWER: AnswerType = 296962999629;

/// The difference between the numbers in the 3-tuple.
const INC: u32 = 3330;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_049::{solution, ANSWER};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	const START: u32 = 1487 + 1;
	const END:   u32 = 10000 - INC;

	let primes = Sieve::new(10000);
	(START..END)
	 .filter(|&n| primes.is_prime(n) && primes.is_prime(n + INC) && primes.is_prime(n + 2 * INC))
	 .filter(are_permutations)
	 .map(concatenate)
	 .next().unwrap()
}

/// Checks if the 3-tuple are all permutations of each other.
fn are_permutations(n: &u32) -> bool
{
	/// Breaks the number into an array of digits.
	fn to_array(n: u32) -> [u32; 4]
	{
		[(n / 1000) % 10, (n / 100) % 10, (n / 10) % 10, n % 10]
	}

	let n = *n;
	let mut a = to_array(n);
	let mut b = to_array(n + INC);
	let mut c = to_array(n + 2 * INC);

	a.sort();
	b.sort();
	c.sort();

	a == b && b == c
}

/// Concatenates the 3-tuple into one number.
fn concatenate(n: u32) -> AnswerType
{
	let n = n as AnswerType;
	let i = INC as AnswerType;

	(n * 100_000_000) + ((n + i) * 10_000) + (n + 2 * i)
}
