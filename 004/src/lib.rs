//! Largest palindrome product
//!
//! # Problem Description.
//!
//! A palindromic number reads the same both ways. The largest palindrome made
//! from the product of two 2-digit numbers is 9009 = 91 × 99.
//!
//! Find the largest palindrome made from the product of two 3-digit numbers.

extern crate utils;
use utils::num::Digits;

/// The type of the answer.
type AnswerType = u32;

/// The answer to the problem.
pub const ANSWER: AnswerType = 906609;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_004::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	// We iterate over the palindromes and check if they're divisible (versus
	// iterating over divisors) for one reason: we can search from largest to
	// smallest.
	//
	// We're also only checking for even length palindromes. Mostly because
	// there's no good way to make the even and odd length checks happen using
	// the same code, the biggest possible palindromes are even, and it does
	// find the answer.
	//
	// The `take_while` should arguably be a `rev` (or some combination), but
	// it works.
	(100..1000).rev().map(create_palindrome)
	                 .flat_map(|p| (100..1000).take_while(move |&n| n * n <= p)
	                                          .map(move |n| (p, n)))
	                 .filter(|&(p, n)| p % n == 0 && p / n >= 100 && p / n < 1000)
	                 .map(|(p, _)| p)
	                 .next().unwrap()
}

/// Creates a palindrome from a number.
fn create_palindrome(n: u32) -> u32
{
	let iter = n.to_digits(10).rev().chain(n.to_digits(10));
	Digits::from_digits(iter, 10).unwrap()
}
