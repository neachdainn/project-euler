//! Combinatoric selections
//!
//! # Problem Description.
//!
//! There are exactly ten ways of selecting three from five, 12345: 123, 124,
//! 125, 134, 135, 145, 234, 235, 245, and 345
//!
//! In combinatorics, we use the notation, *⁵C₃* = 10.
//!
//! In general,
//! *<sup>n</sup>C<sub>r</sub>* = (<i>n</i>!)/(<i>r</i>!(<i>n</i>−<i>r</i>)!),
//! where *r* ≤ *n*, <i>n</i>! = *n* · (<i>n</i>−1) · … · 3 · 2 · 1,
//! and 0! = 1.
//!
//! It is not until *n* = 23, that a value exceeds one-million:
//! *<sup>23</sup>C<sub>10</sub>* = 1144066.
//!
//! How many, not necessarily distinct, values of  *<sup>n</sup>C<sub>r</sub>*,
//! for 1 ≤ *n* ≤ 100, are greater than one-million?


/// The type of the answer.
type AnswerType = usize;

/// The answer to the problem.
pub const ANSWER: AnswerType = 4075;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_053::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	// The major thing to avoid here is recalculating factorials, which means
	// some form of dynamic programming. We could use the memoizer but the
	// equation for the "next" combination is actually fairly simple:
	// nCr₂ = nCr₁ * (n - r)/(r + 1)
	//
	// The problem description tells us that we can skip the first 23 rows. We
	// also only need to find the first item over one million in each row in
	// order to know how many other items in the row are also over one million.
	// Specifically, if the first item over a million is `r`, then there are
	// `n - 2r - 1` other things over one million.
	let next = |&v, n, r| (v * (n - r)) / (r + 1);
	(23..101).map(|n| (0..n).scan(1, |v, r| { *v = next(v, n, r); Some((*v, r)) })
	                        .take_while(|&(v, _)| v <= 1_000_000)
	                        .last()
	                        .map(|(_, r)| n - 2 * r - 3)
	                        .unwrap_or(0))
	         .sum()
}
