//! Cubic permutations
//!
//! # Problem Description.
//!
//! The cube, 41063625 (345³), can be permuted to produce two other cubes:
//! 56623104 (384³) and 66430125 (405³). In fact, 41063625 is the smallest cube
//! which has exactly three permutations of its digits which are also cube.
//!
//! Find the smallest cube for which exactly five permutations of its digits
//! are cube.

extern crate utils;

use std::collections::HashMap;
use utils::num::Digits;

/// The type of the answer.
type AnswerType = i64;

/// The answer to the problem.
pub const ANSWER: AnswerType = 127035954683;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_062::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	// NOTE:
	// This will give the correct answer and runs about 200μs faster if we
	// disregard the "exactly five" bit of the problem description and return
	// the first number to have at least five permutations. However, that isn't
	// a significant enough speedup for me to want to make that change here.
	//
	// Also, there is about a 20μs difference between clearing all entries
	// between digit counts and not (as long as the hash map preallocates the
	// correct size). As such, I'm opting for the version which uses less
	// memory, even if it is only about 350kB less.

	let mut cubes = HashMap::with_capacity(6000);
	let mut prev_num_digits = 0;

	// The first cube to have five digits is 22³
	for c in (22..).map(|x| x * x * x) {
		let (num_digits, dist) = digit_dist(c);

		// If we're in a new class of cubes, we need to check if one met the
		// criteria of _exactly_ five permutations.
		if num_digits != prev_num_digits {
			// Check to see if we found the answer
			let min = cubes.values()
			               .filter(|&&(c, _)| c == 5)
			               .map(|&(_, n)| n)
			               .min();
			if let Some(m) = min {
				return m;
			}

			// Otherwise forget everything
			cubes.clear();
			prev_num_digits = num_digits;
		}

		// Add this number to the map and move on.
		cubes.entry(dist).or_insert((0, c)).0 += 1;
	}

	unreachable!();
}

/// Returns the digital distribution of the number.
///
/// The first value is the total number of digits with the second being the
/// distribution itself. In a "normal" project, I think this would be a funky
/// way to do things, but it should be faster in the Project Euler context.
fn digit_dist(num: AnswerType) -> (usize, [usize; 10])
{
	let mut dist = [0; 10];
	let mut count = 0;
	num.to_digits(10).for_each(|d| { dist[d as usize] += 1; count += 1; });

	(count, dist)
}
