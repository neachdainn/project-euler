//! Maximum path sum I
//!
//! # Problem Description.
//!
//! Find the maximum total from top to bottom of the provided triangle.

extern crate utils;
use utils::itertools::Convolution;

use std::mem;

/// The type of the answer.
type AnswerType = u32;

/// The answer to the problem.
pub const ANSWER: AnswerType = 1074;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_018::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	let triangle = vec![
		vec![75],
		vec![95, 64],
		vec![17, 47, 82],
		vec![18, 35, 87, 10],
		vec![20, 04, 82, 47, 65],
		vec![19, 01, 23, 75, 03, 34],
		vec![88, 02, 77, 73, 07, 63, 67],
		vec![99, 65, 04, 28, 06, 16, 70, 92],
		vec![41, 41, 26, 56, 83, 40, 80, 70, 33],
		vec![41, 48, 72, 33, 47, 32, 37, 16, 94, 29],
		vec![53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14],
		vec![70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57],
		vec![91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48],
		vec![63, 66, 04, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31],
		vec![04, 62, 98, 27, 23, 09, 70, 98, 73, 93, 38, 53, 60, 04, 23],
	];

	max_path(triangle)
}

/// Find the maximum path within the supplied triangle.
pub fn max_path(triangle: Vec<Vec<u32>>) -> u32
{
	let mut prev = vec![0];
	let mut next = Vec::new();

	triangle.iter().for_each(|row| {
		next.clear();

		next.extend(row.iter().zip(Convolution::new(&prev, 2)).map(|(c, p)| c + p.iter().max().unwrap()));
		mem::swap(&mut next, &mut prev);
	});

	*prev.iter().max().unwrap()
}
