//! Maximum path sum II
//!
//! # Problem Description.
//!
//! Find the maximum total from top to bottom of the provided triangle which
//! contains one-hundred rows.

extern crate euler_018;

/// The type of the answer.
type AnswerType = u32;

/// The answer to the problem.
pub const ANSWER: AnswerType = 7273;

/// The provided triangle.
pub const TRIANGLE: &'static str = include_str!("triangle.txt");

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_067::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	// This can probably be made faster by having `euler_018::max_path` take a
	// `FnMut() -> Option<&Vec<u32>>`. This will allow us to only allocate a
	// single vector for the input rather than one per line. That optimization
	// might really not be worth the effort, though.
	let triangle =
		TRIANGLE
		.lines()
		.map(|l| {
			l.split_whitespace()
			 .map(|d| d.parse::<u32>())
			 .collect::<Result<Vec<_>, _>>()
		})
		.collect::<Result<Vec<_>, _>>()
		.unwrap();

	euler_018::max_path(triangle)
}
