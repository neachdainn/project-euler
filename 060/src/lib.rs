//! Prime pair sets
//!
//! # Problem Description.
//!
//! The primes 3, 7, 109, and 673, are quite remarkable. By taking any two
//! primes and concatenating them in any order the result will always be prime.
//! For example, taking 7 and 109, both 7109 and 1097 are prime. The sum of
//! these four primes, 792, represents the lowest sum for a set of four primes
//! with this property.
//!
//! Find the lowest sum for a set of five primes for which any two primes
//! concatenate to produce another prime.

extern crate utils;
use utils::primes::Sieve;

/// The type of the answer.
type AnswerType = u32;

/// The answer to the problem.
pub const ANSWER: AnswerType = 26033;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_060::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	make_chain(Vec::new(), &Sieve::new(10_000)).unwrap().iter().sum()
}

fn make_chain(mut chain: Vec<u32>, primes: &Sieve<u32>) -> Result<Vec<u32>, Vec<u32>>
{
	if chain.len() == 5 { return Ok(chain); }

	// Chain was too short. We need to add another.
	let last = *chain.last().unwrap_or(&0);
	for p in primes.iter().skip_while(|&p| p <= last) {
		// Make sure this number continues to be a prime pair
		let pairs = chain.iter().all(|&x| primes.is_prime_safe(concat(p, x)) && primes.is_prime_safe(concat(x, p)));

		if pairs {
			chain.push(p);

			match make_chain(chain, primes) {
				Err(mut c) => {
					c.pop();
					chain = c;
				},
				Ok(c) => return Ok(c),
			}
		}
	}

	Err(chain)
}

/// Concatenates `a` and `b` into `a||b`
fn concat(a: u32, b: u32) -> u32
{
	let digits = ((b as f64).log10().floor() + 1.0) as u32;
	let multiplier = 10u32.pow(digits);

	a * multiplier + b
}

#[cfg(test)]
mod test
{
	#[test]
	fn concat()
	{
		assert_eq!(super::concat(3, 7), 37);
		assert_eq!(super::concat(7, 3), 73);

		assert_eq!(super::concat(3, 109), 3109);
		assert_eq!(super::concat(109, 3), 1093);

		assert_eq!(super::concat(109, 673), 109673);
		assert_eq!(super::concat(673, 109), 673109);
	}
}
