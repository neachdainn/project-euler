//! Smallest multiple
//!
//! # Problem Description.
//!
//! 2520 is the smallest number that can be divided by each of the numbers from
//! 1 to 10 without any remainder.
//!
//! What is the smallest positive number that is evenly divisible by all of the
//! numbers from 1 to 20?


/// The type of the answer.
type AnswerType = u32;

/// The answer to the problem.
pub const ANSWER: AnswerType = 232792560;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_005::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	// If it is divisible by all numbers 1 through 20, it will be a multiple of
	// the product of all primes less than 20.
	let step = 2 * 3 * 5 * 7 * 11 * 13 * 17 * 19;
	(1..).map(|x| x * step).filter(|x| (1..21).all(|p| x % p == 0)).next().unwrap()
}
