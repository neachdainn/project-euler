#![feature(test)]

extern crate euler_003;
extern crate test;
use test::{Bencher, black_box};

#[bench]
fn solution(b: &mut Bencher)
{
	b.iter(|| black_box(euler_003::solution()))
}
