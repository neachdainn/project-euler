//! Largest prime factor
//!
//! # Problem Description.
//!
//! The prime factors of 13195 are 5, 7, 13 and 29.
//!
//! What is the largest prime factor of the number 600851475143?

extern crate utils;
use utils::primes::Sieve;

/// The type of the answer.
type AnswerType = u64;

/// The answer to the problem.
pub const ANSWER: AnswerType = 6857;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_003::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	// The square root of the number is ~775,000 but it turns out we don't need
	// any factors that large.
	let primes = Sieve::new(10_000);
	primes.factors(600_851_475_143).unwrap().last().map(|&(f, _)| f).unwrap()
}
