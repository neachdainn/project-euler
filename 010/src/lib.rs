//! Summation of primes
//!
//! # Problem Description.
//!
//! The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
//!
//! Find the sum of all the primes below two million.


extern crate utils;
use utils::primes::Sieve;

/// The type of the answer.
type AnswerType = i64;

/// The answer to the problem.
pub const ANSWER: AnswerType = 142913828922;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_010::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	Sieve::new(2_000_000).iter().map(|d| d as i64).sum()
}
