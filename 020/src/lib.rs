//! Factorial digit sum
//!
//! # Problem Description.
//!
//! n! means n × (n − 1) × … × 3 × 2 × 1
//!
//! For example, 10! = 10 × 9 × … × 3 × 2 × 1 = 3628800, and the sum of the
//! digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
//!
//! Find the sum of the digits in the number 100!


extern crate utils;
use utils::num::{BigUInt, Digits};

/// The type of the answer.
type AnswerType = i32;

/// The answer to the problem.
pub const ANSWER: AnswerType = 648;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_020::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	(2u32..101).fold(BigUInt::new(1), |mut acc, d| { acc *= d; acc })
	        .into_digits(10).map(|d| d as i32).sum()
}
