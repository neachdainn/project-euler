extern crate utils;
use utils::LexicographicPerm;

/// What is the millionth lexicographic permutation of the digits 0, 1, 2, 3,
/// 4, 5, 6, 7, 8, and 9?
pub fn solution() -> u32
{
	let elements = vec![9, 8, 7, 6, 5, 4, 3, 2, 1, 0];
	let mut perms = LexicographicPerm::new(elements);

	for _ in 0..999_999 {
		perms.next();
	}
	let final_perm = perms.next().unwrap();

	// Interpret the vector as a number
	final_perm.iter().rev().fold(0, |acc, d| acc * 10 + d)
}

#[cfg(test)]
mod test
{
	#[test]
	fn project_euler_024()
	{
		assert_eq!(super::solution(), 2783915460, "Incorrect solution");
	}
}
