//! Powerful digit sum
//!
//! # Problem Description.
//!
//! A googol (10¹⁰⁰) is a massive number: one followed by one-hundred zeros;
//! 100¹⁰⁰ is almost unimaginably large: one followed by two-hundred zeros.
//! Despite their size, the sum of the digits in each number is only 1.
//!
//! Considering natural numbers of the form, a<sup>b</sup>, where a, b < 100,
//! what is the maximum digital sum?

extern crate utils;
use utils::num::{BigUInt, Digits};

/// The type of the answer.
type AnswerType = u32;

/// The answer to the problem.
pub const ANSWER: AnswerType = 972;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_056::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	(1..100).map(|a| {
		let mut a = BigUInt::new(a);
		let base = a.clone();

		(1..100).map(|_| {
			a *= &base;

			a.to_digits(10).sum::<u32>()
		}).max().unwrap_or(0)
	}).max().unwrap()
}
