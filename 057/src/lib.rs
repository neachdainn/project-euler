//! Square root convergents
//!
//! # Problem Description.
//!
//! It is possible to show that the square root of two can be expressed as an
//! infinite continued fraction.
//!
//! √ 2 = 1 + 1/(2 + 1/(2 + 1/(2 + ... ))) = 1.414213...
//!
//! By expanding this for the first four iterations, we get:
//!
//! * 1 + 1/2 = 3/2 = 1.5
//! * 1 + 1/(2 + 1/2) = 7/5 = 1.4
//! * 1 + 1/(2 + 1/(2 + 1/2)) = 17/12 = 1.41666...
//! * 1 + 1/(2 + 1/(2 + 1/(2 + 1/2))) = 41/29 = 1.41379...
//!
//! The next three expansions are 99/70, 239/169, and 577/408, but the eighth
//! expansion, 1393/985, is the first example where the number of digits in the
//! numerator exceeds the number of digits in the denominator.
//!
//! In the first one-thousand expansions, how many fractions contain a
//! numerator with more digits than denominator?


extern crate utils;
use utils::num::BigUInt;

/// The type of the answer.
type AnswerType = usize;

/// The answer to the problem.
pub const ANSWER: AnswerType = 153;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_057::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	// It turns out,
	// N(k + 1) = N(k) + 2 * D(k)
	// D(k + 1) = N(k + 1) - D(k) = N(k) + D(k)
	let mut num = BigUInt::new(3);
	let mut den = BigUInt::new(2);

	let change_of_base = (10.0f64).log2().recip();

	let mut count = 0;
	for _ in 0..1000 {
		// Faster to add twice than to multiply.
		num += &den;
		num += &den;
		den = &num - den;

		// Converting to a float and then doing log₁₀ is too inaccurate for this
		// to work. Finding the log₂ and then changing the base is accurate
		// enough.
		if (num.log2() * change_of_base).floor() > (den.log2() * change_of_base).floor() {
			count += 1;
		}
	}

	count
}
