//! Prime digit replacements
//!
//! # Problem Description.
//!
//! By replacing the 1st digit of the 2-digit number *3, it turns out that six
//! of the nine possible values: 13, 23, 43, 53, 73, and 83, are all prime.
//!
//! By replacing the 3rd and 4th digits of 56**3 with the same digit, this
//! 5-digit number is the first example having seven primes among the ten
//! generated numbers, yielding the family: 56003, 56113, 56333, 56443, 56663,
//! 56773, and 56993. Consequently 56003, being the first member of this
//! family, is the smallest prime with this property.
//!
//! Find the smallest prime which, by replacing part of the number (not
//! necessarily adjacent digits) with the same digit, is part of an eight prime
//! value family.

extern crate utils;
use utils::LexicographicPerm;
use utils::primes::Sieve;
use utils::num::Digits;

/// The type of the answer.
type AnswerType = u32;

/// The answer to the problem.
pub const ANSWER: AnswerType = 121313;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_051::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	// There are only ten digits, so one of the substitutions must be 0, 1, or
	// 2. This means that we can abort checking if none of those three
	// substitutions meet the criteria.
	//
	// So, now we have to figure out how many digits to repeat or how many
	// total digits are in the number.
	//
	// The non-changing digits will add up to some value modulo 3. We can use
	// this information to determine how many of the substitutions will be
	// divisible by three and therefore not prime.
	//
	// |   | 1 | 2 | 3  | 4 | 5 | 6  | 7 | 8 | 9  | 10 |
	// |---|---|---|----|---|---|----|---|---|----|----|
	// | 0 | 4 | 4 | 10 | 4 | 4 | 10 | 4 | 4 | 10 |  4 |
	// | 1 | 3 | 3 |  0 | 3 | 3 |  0 | 3 | 3 |  0 |  3 |
	// | 2 | 3 | 3 |  0 | 3 | 3 |  0 | 3 | 3 |  0 |  3 |
	//
	// Now, I don't have a proof that this pattern continues forever but I
	// would bet that it does. But this means that the number of repeat digits
	// needs to be a multiple of three, otherwise only seven umbers can be
	// prime. It also means that the non-repeating digits must not have a sum
	// that is divisible by three.
	//
	// We also know that all prime numbers greater than ten end in one of 1, 3,
	// 7, or 9. This means that the last digit cannot be one of the repeating
	// numbers.
	//
	// I don't know if there is anything else we can analyze out of this, but
	// that should be enough to dramatically reduce the search space for the
	// brute force algorithm.
	//
	// This code is kind of gross. But I had to break it down like this
	// otherwise I just couldn't wrap my head around the solution for some
	// reason. On the bright side, it did help with unit testing.
	let primes = Sieve::new(1_000_000);

	// We know that it has to be at least five digits but I couldn't analyze an
	// exact number.
	(5..).filter_map(|n| try_find_answer(n, &primes)).next().unwrap()
}

/// Attempts to find the answer in the set of numbers with the given length.
fn try_find_answer(digits: u32, primes: &Sieve<u32>) -> Option<AnswerType>
{
	// We need to determine how many numbers to substitute. It has to be a
	// multiple of three and there has to be at least one remaining digit for
	// the last item.
	(1..).map(|x| 3 * x).take_while(|&x| x < digits).filter_map(|x| check_substitutions(digits, x, primes)).next()
}

/// Determines if the answer can be found using the specified number of digits
/// and the specified number of substitutions.
fn check_substitutions(digits: u32, substitutions: u32, primes: &Sieve<u32>) -> Option<AnswerType>
{
	let mut perms = {
		// Subtract one digit from the permutations, since the smallest number
		// cannot be a substitution.
		let rep_vec = (0..digits - 1).map(|n| n < substitutions).collect();
		LexicographicPerm::new(rep_vec)
	};

	let num_fixed_digits = digits - substitutions;
	let digit_range_low = (10 as AnswerType).pow(num_fixed_digits);
	let digit_range_high = digit_range_low * 10;

	while let Some(p) = perms.next() {
		for num in (digit_range_low..digit_range_high).filter(|&n| n % 3 != 0) {
			if let Some(answer) = check_permutation(num, p, primes) {
				return Some(answer);
			}
		}
	}

	None
}

/// Applies the fixed number to the permutation and checks to see if it is in a
/// prime family.
fn check_permutation(fixed: AnswerType, permutation: &[bool], primes: &Sieve<u32>) -> Option<AnswerType>
{
	let mut res = None;
	let mut count = 0;

	for sub in 0..10 {
		// If the most significant digit is supposed to be substituted, it
		// cannot be substituted with zero.
		if *permutation.last().unwrap() && sub == 0 {
			continue;
		}

		// Reconstruct the number from the permutation and the fixed numbers
		let num = reconstruct(fixed, sub, permutation);
		if primes.is_prime(num) {
			count += 1;
			res = res.or(Some(num));
		}

		if count + 1 < sub {
			res = None;
			break;
		}
	}

	res
}

/// Reconstructs a number based on the fixed digits, the permutation, and the
/// substitute number.
///
/// The smallest digit will always be the smallest digit of the fixed numbers.
/// The remaining digits are interspersed with the substitute number based on
/// the permutation array.
fn reconstruct(fixed: AnswerType, sub: AnswerType, permutation: &[bool]) -> AnswerType
{
	let mut digits = fixed.to_digits(10);
	let smallest = digits.next().unwrap();

	permutation.iter().scan(10, |acc, &s| {
		let n = if s { sub } else { digits.next().unwrap() };
		let v = *acc * n;

		*acc *= 10;
		Some(v)
	}).sum::<AnswerType>() + smallest
}

#[cfg(test)]
mod test
{
	#[test]
	fn reconstruct()
	{
		let num = super::reconstruct(1235, 4, &[false, true, false, true, false, true, true]);
		assert_eq!(num, 44142435);

		let num = super::reconstruct(1234, 4, &[false, false, false]);
		assert_eq!(num, 1234);

		let num = super::reconstruct(1235, 4, &[true, true, true, true]);
		assert_eq!(num, 44445);
	}
}
