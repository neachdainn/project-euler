//! Counting Sundays
//!
//! # Problem Description.
//!
//! You are given the following information, but you may prefer to do some research for yourself.
//!
//! * 1 Jan 1900 was a Monday.
//! * Thirty days has September, <br/>
//!   April, June and November. <br/>
//!   All the rest have thirty-one, <br/>
//!   Saving February alone, <br/>
//!   Which has twenty-eight, rain or shine. <br/>
//!   And on leap years, twenty-nine.
//! * A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.
//!
//! How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?

extern crate utils;
use utils::chrono::{Date, Month, DayOfWeek};

/// The type of the answer.
type AnswerType = usize;

/// The answer to the problem.
pub const ANSWER: AnswerType = 171;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_019::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	// I would imagine that I could turn this into a math thing rather than a
	// counting thing, but here we are for now.
	(0..).scan(Date::new(1901, Month::January, 1), |acc, _| {
		let nmonth = acc.month().next_month();
		let year = if nmonth == Month::January { acc.year() + 1 } else { acc.year() };

		let current = *acc;
		*acc = Date::new(year, nmonth, 1);

		Some(current)
	}).take_while(|&d| d <= Date::new(2000, Month::December, 31))
	  .filter(|d| d.day_of_week() == DayOfWeek::Sunday)
	  .count()
}
