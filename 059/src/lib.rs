//! XOR decryption
//!
//! # Problem Description.
//!
//! Each character on a computer is assigned a unique code and the preferred
//! standard is ASCII (American Standard Code for Information Interchange). For
//! example, uppercase A = 65, asterisk (*) = 42, and lowercase k = 107.
//!
//! A modern encryption method is to take a text file, convert the bytes to
//! ASCII, then XOR each byte with a given value, taken from a secret key. The
//! advantage with the XOR function is that using the same encryption key on
//! the cipher text, restores the plain text; for example, 65 XOR 42 = 107,
//! then 107 XOR 42 = 65.
//!
//! For unbreakable encryption, the key is the same length as the plain text
//! message, and the key is made up of random bytes. The user would keep the
//! encrypted message and the encryption key in different locations, and
//! without both "halves", it is impossible to decrypt the message.
//!
//! Unfortunately, this method is impractical for most users, so the modified
//! method is to use a password as a key. If the password is shorter than the
//! message, which is likely, the key is repeated cyclically throughout the
//! message. The balance for this method is using a sufficiently long password
//! key for security, but short enough to be memorable.
//!
//! Your task has been made easy, as the encryption key consists of three lower
//! case characters. Using cipher.txt, a file containing the encrypted ASCII
//! codes, and the knowledge that the plain text must contain common English
//! words, decrypt the message and find the sum of the ASCII values in the
//! original text.


extern crate utils;

/// The type of the answer.
type AnswerType = u32;

/// Key to the cipher.
type Key = [u8; 3];

/// `cipher.txt`
pub const CIPHER: &'static str = include_str!("cipher.txt");

/// The answer to the problem.
pub const ANSWER: AnswerType = 107359;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_059::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	let key = find_key();
	CIPHER[..CIPHER.len() - 1].split(',')
	                          .map(|s| s.parse::<u8>().unwrap())
	                          .enumerate()
	                          .map(|(e, c)| (c ^ key[e % 3]) as u32)
	                          .sum::<u32>()
}

/// Analyze the cipher text to determine the key.
///
/// We know enough about the key and encryption to just figure out which
/// character is space and then move on from there.
fn find_key() -> Key
{
	let mut counts = [[0; 256]; 3];

	// Skip the last item since it is a newline.
	for (e, b) in CIPHER[..CIPHER.len() - 1].split(',').map(|c| c.parse::<u8>().unwrap()).enumerate() {
		counts[e % 3][b as usize] += 1
	}

	[
	counts[0].iter().enumerate().max_by_key(|&(_, c)| c).map(|(e, _)| e as u8).unwrap() ^ b' ',
	counts[1].iter().enumerate().max_by_key(|&(_, c)| c).map(|(e, _)| e as u8).unwrap() ^ b' ',
	counts[2].iter().enumerate().max_by_key(|&(_, c)| c).map(|(e, _)| e as u8).unwrap() ^ b' ',
	]
}
