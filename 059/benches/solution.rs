#![feature(test)]

extern crate euler_059;
extern crate test;
use test::{Bencher, black_box};

#[bench]
fn solution(b: &mut Bencher)
{
	b.iter(|| black_box(euler_059::solution()))
}
