//! Consecutive prime sum
//!
//! # Problem Description.
//!
//! The prime 41, can be written as the sum of six consecutive primes:
//!
//! 41 = 2 + 3 + 5 + 7 + 11 + 13
//!
//! This is the longest sum of consecutive primes that adds to a prime below
//! one-hundred.
//!
//! The longest sum of consecutive primes below one-thousand that adds to a
//! prime, contains 21 terms, and is equal to 953.
//!
//! Which prime, below one-million, can be written as the sum of the most
//! consecutive primes?

extern crate utils;
use utils::primes::Sieve;

/// The type of the answer.
type AnswerType = u64;

/// The answer to the problem.
pub const ANSWER: AnswerType = 997651;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_050::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	let primes = Sieve::new(1_000_000);
	let sums: Vec<_> = primes.iter().scan(0u64, |acc, p| {*acc += p as u64; Some(*acc)}).collect();

	let mut longest_chain = 21;
	let mut longest_chain_value = 953;

	// What we're doing is iterating over the sum of the primes and subtracting
	// the smaller sums to test whether or not the result is a prime.
	for (i, s) in sums.iter().enumerate().skip(longest_chain) {
		// We only want to start subtracting the values if it results in a
		// chain longer than we've seen before. Anything else is a waste of
		// time.
		for j in (0..i - longest_chain).rev() {
			let candidate = s - sums[j];

			// If the candidate is larger than one million, it will only get
			// larger as we subtract smaller and smaller things from it.
			if candidate > 1_000_000 {
				break;
			}

			if primes.is_prime(candidate) {
				longest_chain = i - j;
				longest_chain_value = candidate;
			}
		}
	}

	longest_chain_value
}
