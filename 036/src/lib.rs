extern crate utils;

use utils::num::is_palindrome;

/// Find the sum of all numbers, less than one million, which are palindromic
/// in base 10 and base 2.
pub fn solution() -> u32
{
	(0..1_000_000).filter(|&n| is_palindrome(n, 10) && is_palindrome(n, 2)).sum()
}

#[cfg(test)]
mod test
{
	#[test]
	fn project_euler_036()
	{
		assert_eq!(super::solution(), 872187, "Incorrect solution");
	}
}
