//! Number letter counts
//!
//! # Problem Description.
//!
//! If the numbers 1 to 5 are written out in words: one, two, three, four,
//! five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
//!
//! If all the numbers from 1 to 1000 (one thousand) inclusive were written out
//! in words, how many letters would be used?
//!
//! NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and
//! forty-two) contains 23 letters and 115 (one hundred and fifteen) contains
//! 20 letters. The use of "and" when writing out numbers is in compliance with
//! British usage.

/// The type of the answer.
type AnswerType = usize;

/// The answer to the problem.
pub const ANSWER: AnswerType = 21124;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_017::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	(1..1001).map(num_to_english)
	         .flat_map(|s| s.into_bytes())
	         .filter(|&b| b >= b'a' && b <= b'z')
	         .count()
}

/// Converts a number to its English representation.
///
/// Currently only implemented for numbers greater than zero and less than
/// one-thousand.
fn num_to_english(num: i32) -> String
{
	assert!(num > 0 && num <= 1000, "number out of implemented range");
	if num == 1000 { return String::from("one thousand"); }

	let ones = num % 10;
	let tens = num % 100 / 10;
	let hundreds = num % 1000 / 100;

	let lower = match (tens, ones) {
		(0, 0) => String::new(),
		(0, d) => digit_to_english(d),
		(1, 0) => String::from("ten"),
		(1, 1) => String::from("eleven"),
		(1, 2) => String::from("twelve"),
		(1, 4) => String::from("fourteen"),
		(1, d) => digit_to_modified(d) + "teen",
		(t, 0) => digit_to_modified(t) + "ty",
		(t, d) => digit_to_modified(t) + "ty-" + &digit_to_english(d),
	};

	match (hundreds, tens, ones) {
		(0, _, _) => lower,
		(h, 0, 0) => digit_to_english(h) + " hundred",
		(h, _, _) => digit_to_english(h) + " hundred and " + &lower,
	}
}

/// Converts a digit to its English representation.
///
/// The number provided must be greater than zero and less than ten.
fn digit_to_english(dig: i32) -> String
{
	match dig {
		1 => String::from("one"),
		2 => String::from("two"),
		3 => String::from("three"),
		4 => String::from("four"),
		5 => String::from("five"),
		6 => String::from("six"),
		7 => String::from("seven"),
		8 => String::from("eight"),
		9 => String::from("nine"),
		_ => unimplemented!(),
	}
}

/// Converts the digit to its modified English form.
fn digit_to_modified(dig: i32) -> String
{
	match dig {
		1 => String::from("ten"),
		2 => String::from("twen"),
		3 => String::from("thir"),
		4 => String::from("for"),
		5 => String::from("fif"),
		6 => String::from("six"),
		7 => String::from("seven"),
		8 => String::from("eigh"),
		9 => String::from("nine"),
		_ => unimplemented!(),
	}
}

#[cfg(test)]
mod test
{
	#[test]
	fn number_conversion()
	{
		assert_eq!(super::num_to_english(342), String::from("three hundred and forty-two"));
		assert_eq!(super::num_to_english(115), String::from("one hundred and fifteen"));
	}
}
