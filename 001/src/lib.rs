//! Multiple of 3 and 5
//!
//! # Problem Description.
//!
//! If we list all the natural numbers below 10 that are multiples of 3 or 5,
//! we get 3, 5, 6 and 9. The sum of these multiples is 23.
//!
//! Find the sum of all the multiples of 3 or 5 below 1000.

/// The type of the answer.
type AnswerType = u32;

/// The answer to the problem.
pub const ANSWER: AnswerType = 233168;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_001::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	(1..1000).filter(|x| x % 3 == 0 || x % 5 == 0).sum()
}
