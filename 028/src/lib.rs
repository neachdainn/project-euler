/// What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral?
pub fn solution() -> u64
{
	// We need to figure out how many entries are in a 1001 by 1001 spiral.
	let limit = 2 * 1001 - 1;

	Diagonals::new().take(limit).map(|(x, _)| x).sum()
}

/// An iterator over the diagonal numbers.
pub struct Diagonals
{
	current: u64,
	increments: [u64; 4],
}
impl Diagonals
{
	/// Create a new iterator over the diagonal numbers.
	pub fn new() -> Self
	{
		Diagonals { current: 1, increments: [2, 2, 2, 2], }
	}
}
impl Iterator for Diagonals
{
	type Item = (u64, u64);

	fn next(&mut self) -> Option<Self::Item>
	{
		let current = self.current;
		let increment = self.increments[0];

		self.current += increment;
		self.increments[0] += 2;

		rotate_slice(&mut self.increments);

		Some((current, self.increments[2] - 1))
	}
}

/// Rotate a slice in place.
fn rotate_slice<T>(s: &mut [T])
{
	for i in 0..s.len() - 1 {
		s.swap(i, i + 1);
	}
}

#[cfg(test)]
mod test
{
	#[test]
	fn project_euler_028()
	{
		assert_eq!(super::solution(), 669171001, "Incorrect solution");
	}

	#[test]
	fn test_swap()
	{
		let mut a = [1, 2, 3, 4];

		super::rotate_slice(&mut a);
		assert_eq!(a, [2, 3, 4, 1]);

		super::rotate_slice(&mut a);
		assert_eq!(a, [3, 4, 1, 2]);
	}

	#[test]
	fn test_diagonals()
	{
		let diags = super::Diagonals::new().take(9).collect::<Vec<_>>();

		assert_eq!(diags, vec![(1, 1), (3, 3), (5, 3), (7, 3), (9, 3), (13, 5), (17, 5), (21, 5), (25, 5)]);
	}
}
