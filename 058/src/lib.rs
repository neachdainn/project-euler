//! Spiral primes
//!
//! # Problem Description.
//!
//! It is interesting to note that the odd squares lie along the bottom right
//! diagonal, but what is more interesting is that 8 out of the 13 numbers
//! lying along both diagonals are prime; that is, a ratio of 8/13 ≈ 62%.
//!
//! If one complete new layer is wrapped around the spiral above, a square
//! spiral with side length 9 will be formed. If this process is continued,
//! what is the side length of the square spiral for which the ratio of primes
//! along both diagonals first falls below 10%?

extern crate euler_028;
extern crate utils;

use euler_028::Diagonals;
use utils::primes::is_prime;

/// The type of the answer.
type AnswerType = u32;

/// The answer to the problem.
pub const ANSWER: AnswerType = 26241;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_058::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	Diagonals::new()
	.enumerate()
	.skip(1)
	.map(|(e, (n, l))| (e + 1, is_prime(n), l))
	.scan(0, |acc, (e, p, l)| {
		if p { *acc += 1 }
		Some((*acc as f64 / e as f64, l))
	})
	.skip_while(|&(r, _)| r > 0.1)
	.next().map(|(_, l)| l as u32).unwrap()
}
