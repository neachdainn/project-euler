//! 10001st prime
//!
//! # Problem Description.
//!
//! By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see
//! that the 6th prime is 13.
//!
//! What is the 10,001st prime number?


extern crate utils;
use utils::primes::Sieve;

/// The type of the answer.
type AnswerType = u32;

/// The answer to the problem.
pub const ANSWER: AnswerType = 104743;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_007::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	// There is a prime counting function we can use to come up with a reaonsable upper limit.
	// π(10⁵) = 9,592
	// π(10⁶) = 78,498
	//
	// There's a huge gap there, so let's try and guess a little better.
	// π(2·10⁵) = 17,984
	// π(1.1·10⁵) = 10,453
	//
	// I'm not sure how I feel about using a table to look these up, but they
	// can be approximated using π(x) ≅ x / ln(x)
	Sieve::new(110_000).iter().nth(10_000).unwrap()
}
