extern crate utils;

use utils::primes::SieveOfEratosthenes as SoE;

/// How many circular primes are there below one million?
pub fn solution() -> usize
{
	const LIMIT: usize = 1_000_000;
	let primes = SoE::new(LIMIT);

	(0..LIMIT).filter(|&n| Rotations::new(n).all(|n| primes.is_prime(n))).count()
}

/// Iterates over the rotations of a number.
struct Rotations
{
	original: usize,
	current: Option<usize>,
	upper: usize,
}
impl Rotations
{
	fn new(num: usize) -> Self
	{
		Rotations {
			original: num,
			current: Some(num),
			upper: 10usize.pow((num as f64).log10().trunc() as u32),
		}
	}
}
impl Iterator for Rotations
{
	type Item = usize;

	fn next(&mut self) -> Option<Self::Item>
	{
		if let Some(c) = self.current {
			let next = ((c % 10) * self.upper) + (c / 10);
			self.current = if next != self.original { Some(next) } else { None };
			Some(c)
		} else { None }
	}
}

#[cfg(test)]
mod test
{
	#[test]
	fn project_euler_035()
	{
		assert_eq!(super::solution(), 55, "Incorrect solution");
	}

	#[test]
	fn rotations()
	{
		let rots = super::Rotations::new(197).collect::<Vec<_>>();
		assert_eq!(rots, vec![197, 719, 971]);
	}
}
