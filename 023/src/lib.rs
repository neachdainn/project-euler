extern crate utils;
use utils::mem::Memoizer;

/// All numbers above this can be written as the product of two abundant
/// numbers.
const LIMIT: u32 = 28123;

/// Find the sum of all the positive integers which cannot be written as the
/// sum of two abundant numbers.
pub fn solution() -> u32
{
	let abundant = Memoizer::new(|_, num| is_abundant(num));

	(1..LIMIT).filter(|&d| {
		// 12 is given as the smallest abundant number
		!(12..d).any(|n| abundant.call(n) && abundant.call(d - n))
	}).sum()
}

fn is_abundant(num: u32) -> bool
{
	(2..num).take_while(|&d| num / d >= d)
	        .filter(|&d| num % d == 0)
	        .map(|d| if num / d == d { d } else { d + (num / d) })
	        .sum::<u32>() + 1 > num
}

#[cfg(test)]
mod test
{
	#[test]
	fn project_euler_023()
	{
		assert_eq!(super::solution(), 4179871, "Incorrect solution");
	}

	#[test]
	fn abundant()
	{
		assert!(super::is_abundant(12));
	}
}
