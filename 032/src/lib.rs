/// Find the sum of all products whose multiplicand/multiplier/product identity
/// can be written as a 1 through 9 pandigital.
pub fn solution() -> u64
{
	// We need to define a reasonable search space for this problem.
	//
	// We know that there must be a total of nine digits involved in this
	// problem. We know that `x * y = z` and `||x|| + ||y|| + ||z|| = 9`. This
	// means that `⌊log10(x)⌋ + 1 + ⌊log10(y)⌋ + 1 + ⌊log10(x * y)⌋ + 1 = 9`,
	// or `⌊log10(x)⌋ + ⌊log10(y)⌋ + ⌊log10(x * y)⌋ = 6`.
	//
	// We translate this to `6 ≤ log10(x) + log10(y) + log10(x * y) ≤ 9` so
	// that Wolfram Alpha can simplify it. The result is:
	// `1000/x ≤ y ≤ 10000 * √10 * √(1 / x²)`
	//
	// Now, this gives us a reasonable range for `x`. It must be at least 1,
	// but any more than 1000 doesn't make a lot of sense.
	let mut pandigits = (1..1001u64).flat_map(|x| (1000/x..y_upper_limit(x)).map(move |y| (x, y)))
	                                .filter(is_pandigital)
	                                .map(|(x, y)| x * y)
	                                .collect::<Vec<_>>();

	pandigits.sort();
	pandigits.dedup();

	pandigits.iter().sum()
}

/// Checks if the multiplicand/multiplier/product set is pandigital.
fn is_pandigital(&(a, b): &(u64, u64)) -> bool
{
	let mut nums = [0; 10];

	// NLL stuff
	{
		let mut update = |d| {
			(0..).scan(d, |acc, _| {
				if *acc == 0 { return None; }
				let v = *acc % 10;
				*acc /= 10;

				Some(v)
			}).for_each(|v| nums[v as usize] += 1);
		};

		update(a);
		update(b);
		update(a * b);
	}

	nums[0] == 0 && nums.iter().skip(1).all(|&v| v == 1)
}

/// Calculates the upper limit of `y` given `x`.
fn y_upper_limit(x: u64) -> u64
{
	const SQRT_10: f64 = 3.1622776601683795227870632516E0;
	(10000.0 * SQRT_10 * f64::sqrt(f64::recip((x as f64).powi(2))) + 1.0) as u64
}

#[cfg(test)]
mod test
{
	#[test]
	fn project_euler_032()
	{
		assert_eq!(super::solution(), 45228, "Incorrect solution");
	}

	#[test]
	fn pandigital()
	{
		assert!(super::is_pandigital(&(39, 186)));
	}

	#[test]
	fn not_pandigital()
	{
		assert!(!super::is_pandigital(&(1, 234567)));
	}
}
