extern crate utils;

use utils::primes::SieveOfEratosthenes as SoE;

/// What is the smallest odd composite that cannot be written as the sum of a
/// prime and twice a square?
pub fn solution() -> u32
{
	// The plan is to take all of the twice squares, subtract that from the
	// number, and checking if the remainder is prime. We do it this direction
	// (rather than subtracting the prime) since checking against the sieve
	// will be faster than checking against a HashSet.
	let primes = SoE::new(10_000);
	let odd_composites = (11..).filter(|&n| n % 2 == 1 && !primes.is_prime(n));

	for num in odd_composites {
		let check = (1..).map(|n| 2 * n * n)
		                 .take_while(|&n| n < num)
		                 .any(|n| primes.is_prime(num - n));

		if !check {
			return num;
		}
	}

	unreachable!();
}

#[cfg(test)]
mod test
{
	#[test]
	fn project_euler_046()
	{
		assert_eq!(super::solution(), 5777, "Incorrect solution");
	}
}
