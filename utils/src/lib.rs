extern crate num as num_crate;
extern crate smallvec;
extern crate fnv;

/// Items that have to do with the properties of numbers.
pub mod num;

/// Tools for handling the Fibonacci sequence.
pub mod fib;

/// Prime generation and testing.
pub mod primes;

/// Pythagorean triples.
pub mod ptriples;
pub use ptriples::PythagoreanTriples;

/// Iterator utilities.
pub mod itertools;

/// Date and time utilities.
pub mod chrono;

/// Memoizers.
pub mod mem;

/// Lexicographic permutations.
mod lexicographic;
pub use lexicographic::LexicographicPerm;
