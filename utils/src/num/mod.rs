mod bignum;
pub use self::bignum::{BigInt, BigUInt};

mod digits;
pub use self::digits::{Digits, FromDigitError};

// Traits from Num
pub use num_crate::{Num, One, PrimInt, Unsigned, Signed};

/// Returns the modulo power of a number.
pub fn modpow<T: PrimInt>(num: T, mut exp: T, modulus: T) -> T
{
	let _0 = T::zero();
	let _1 = T::one();
	let _2 = _1 + _1;

	if modulus == _1 { return _0; }

	let mut result = _1;
	let mut base = num % modulus;

	while exp > _0 {
		if exp % _2 == _1 {
			result = (result * base) % modulus;
		}

		exp = exp >> 1;
		base = (base * base) % modulus;
	}

	result
}

/// Returns the modulo power of a number.
///
/// This version runs significantly slower than the `modpow` version, but it is
/// less likely to overflow.
pub fn modpow_safe<T: PrimInt>(num: T, mut exp: T, modulus: T) -> T
{
	let _1 = T::one();
	let _0 = T::zero();

	let mut res = _1;
	while exp > _0 {
		res = (res * num) % modulus;
		exp = exp - _1;
	}

	res
}

/// Returns true if the supplied number is a palindrome number.
pub fn is_palindrome<T: PrimInt>(num: T, base: T) -> bool
{
	// TODO:
	// Have this just take `T: Digits`, as that's all that is really needed
	let (forward, len) = {
		let f = num.to_digits(base);
		let len = f.len();
		(f.take(len / 2), len)
	};
	let backward = num.to_digits(base).rev().take(len / 2);

	// This is slightly inefficient, since we really could stop half way
	// through, but we're looping through powers of `base`, I don't think it
	// really matters.
	forward.eq(backward)
}

/// Returns true if the supplied number is pandigital one through nine.
pub fn is_pandigital<T: PrimInt>(num: &T) -> bool
{
	let mut seen = [0; 10];

	// Count the digits
	for d in num.to_digits(T::from(10).unwrap()) {
		seen[d.to_usize().unwrap()] += 1;
	}

	seen[0] == 0 && seen.iter().skip(1).all(|&c| c == 1)
}

#[cfg(test)]
mod test
{
	#[test]
	fn palindrome_number()
	{
		assert!(super::is_palindrome(123321, 10));
		assert!(super::is_palindrome(9001009, 10));
		assert!(super::is_palindrome(0b1101011, 2));
	}

	#[test]
	fn not_palindrome_number()
	{
		assert!(!super::is_palindrome(12332, 10));
		assert!(!super::is_palindrome(901009, 10));
		assert!(!super::is_palindrome(0b11001, 2));
	}
}
