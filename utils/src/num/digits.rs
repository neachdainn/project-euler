use itertools::IntoDoubleEndedIterator;
use num::PrimInt;

/// A trait for iterating over the digits of a number.
pub trait Digits
	where Self: Sized
{
	type Elem;
	type Iter: Iterator<Item=Self::Elem>;

	/// Iterates over the digits in the given base.
	fn to_digits(&self, base: Self::Elem) -> Self::Iter;

	/// Iterates over the digits in the given base.
	fn into_digits(self, base: Self::Elem) -> Self::Iter;

	/// Reconstruct the number from an iterator.
	fn from_digits<I>(iter: I, base: Self::Elem) -> Result<Self, FromDigitError>
		where I: IntoDoubleEndedIterator<Item=Self::Elem>;
}

impl<T> Digits for T
	where T: PrimInt
{
	type Elem = T;
	type Iter = Iter<T>;

	fn to_digits(&self, base: Self::Elem) -> Self::Iter
	{
		// FIXME:
		// Oh god, why?
		let num = if *self < T::zero() { !(*self) + T::one() } else { *self };
		Iter::new(num, base)
	}

	fn into_digits(self, base: Self::Elem) -> Self::Iter
	{
		Self::to_digits(&self, base)
	}

	fn from_digits<I>(iter: I, base: T) -> Result<T, FromDigitError>
		where I: IntoDoubleEndedIterator<Item=T>
	{
		let mut mag = Some(T::one());
		let mut num = T::zero();

		for i in iter {
			if i >= base {
				return Err(FromDigitError::InvalidDigit);
			}

			if let Some(d) = mag.and_then(|m| i.checked_mul(&m)) {
				num = num + d;
				mag = mag.and_then(|m| m.checked_mul(&base));
			} else { return Err(FromDigitError::DigitOverflow)}
		}

		Ok(num)
	}
}

/// Describes an error that happens when reconstructing a number from digits.
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum FromDigitError
{
	/// A digit was encountered that was too large for the base.
	InvalidDigit,

	/// There were too many digits to reconstruct the number.
	DigitOverflow,
}

/// An implementation of the Digits iterator for all primitive integers.
pub struct Iter<T>
{
	num: T,
	base: T,

	leading_zeros: usize,
}
impl<T> Iter<T>
	where T: PrimInt
{
	fn new(num: T, base: T) -> Self
	{
		Iter { num, base, leading_zeros: 0 }
	}

	fn poor_mans_int_log(mut num: T, base: T) -> (T, usize)
	{
		// Using log gave me some issues. A table won't work over generics. So
		// we need to loop. Plus, the underlying log implementations loop
		// anyway, so this shouldn't really matter.
		let (mut mult, mut cnt) = if num > T::zero() { (T::one(), 1) } else { (T::zero(), 0) };
		while num >= base {
			// This is awkard, but we need to be able to get the correct
			// multiplie without overestimating.
			mult = mult * base;
			cnt += 1;
			num = num / base;
		}

		(mult, cnt)
	}
}
impl<T> Iterator for Iter<T>
	where T: PrimInt
{
	type Item = T;

	fn next(&mut self) -> Option<Self::Item>
	{
		// Check to see if we have any leading zeros to output
		if self.num == T::zero() {
			if self.leading_zeros == 0 {
				return None
			}
			else {
				self.leading_zeros -= 1;
				return Some(T::zero())
			}
		}

		// If the number isn't zero, output the least significant digit.
		let v = self.num % self.base;
		self.num = self.num / self.base;
		Some(v)
	}

	fn size_hint(&self) -> (usize, Option<usize>)
	{
		let (_, digits) = Iter::poor_mans_int_log(self.num, self.base);
		let num_digits = digits + self.leading_zeros;

		(num_digits as usize, Some(num_digits as usize))
	}
}
impl<T> DoubleEndedIterator for Iter<T>
	where T: PrimInt
{
	fn next_back(&mut self) -> Option<Self::Item>
	{
		// Remove any leading zeros as needed
		if self.leading_zeros > 0 {
			self.leading_zeros -= 1;
			return Some(T::zero());
		}

		// Check to see if we're done
		if self.num == T::zero() {
			return None;
		}

		// Otherwise, pop off the last digit
		let (multiplier, num_digits) = Iter::poor_mans_int_log(self.num, self.base);
		let v = (self.num / multiplier) % self.base;

		// Figure out the remaining number
		self.num = self.num % multiplier;

		// Count the number of leading zeros
		let (_, new_num_digits) = Iter::poor_mans_int_log(self.num, self.base);
		self.leading_zeros = num_digits - new_num_digits - 1;

		Some(v)
	}
}
impl<T: PrimInt> ExactSizeIterator for Iter<T> {}

#[cfg(test)]
mod test
{
	use super::*;

	#[test]
	fn twos_complement()
	{
		let a: i32 = 1234;
		assert_eq!(-a, !a + 1);
	}

	#[test]
	fn digits()
	{
		let res = 1025601.into_digits(10).collect::<Vec<_>>();
		assert_eq!(res, vec![1, 0, 6, 5, 2, 0, 1]);
	}

	#[test]
	fn from_digits()
	{
		let digits = vec![1, 0, 6, 5, 2, 0, 1];
		let num: u32 = Digits::from_digits(digits, 10).unwrap();
		assert_eq!(num, 1025601);
	}

	#[test]
	fn digits_rev()
	{
		let res = 1025601.into_digits(10).rev().collect::<Vec<_>>();
		assert_eq!(res, vec![1, 0, 2, 5, 6, 0, 1]);

		println!("\n\n\n\n");
		let res = 11000.into_digits(10).rev().collect::<Vec<_>>();
		assert_eq!(res, vec![1, 1, 0, 0, 0]);
	}

	#[test]
	fn digits_mixed()
	{
		let mut iter = 10025601.into_digits(10);

		assert_eq!(iter.next(), Some(1));
		assert_eq!(iter.next(), Some(0));
		assert_eq!(iter.next_back(), Some(1));
		assert_eq!(iter.next(), Some(6));
		assert_eq!(iter.next_back(), Some(0));
		assert_eq!(iter.next(), Some(5));
		assert_eq!(iter.next(), Some(2));
		assert_eq!(iter.next(), Some(0));
		assert_eq!(iter.next_back(), None);
		assert_eq!(iter.next(), None);
	}
}
