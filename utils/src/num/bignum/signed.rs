use std::ops::*;
use std::cmp::Ordering;
use std::fmt;
use num::BigUInt;

/// Arbitrarily sized signed integer type.
#[derive(PartialEq, Eq, Clone, Debug, Hash)]
pub struct BigInt
{
	magnitude: BigUInt,
	signum: i32,
}
impl BigInt
{
	/// Create a new `BigInt` from the specified primitive.
	pub fn new(val: i32) -> Self
	{
		let inner = val.abs() as u32;
		let signum = val.signum();

		BigInt {
			magnitude: BigUInt::new(inner),
			signum
		}
	}

	/// Returns true if this is zero.
	pub fn is_zero(&self) -> bool
	{
		self.magnitude.is_zero()
	}

	/// Returns this number as an `i32`, if possible.
	pub fn as_i32(&self) -> Option<i32>
	{
		self.magnitude
			.as_u32()
			.and_then(|x| {
				if x > ::std::i32::MAX as u32 {
					None
				}
				else {
					(x as i32).checked_mul(self.signum)
				}
			})
	}
}

impl Add<i32> for BigInt
{
	type Output = BigInt;

	fn add(mut self, rhs: i32) -> Self::Output
	{
		self += rhs;
		self
	}
}
impl Add<BigInt> for i32
{
	type Output = BigInt;

	fn add(self, mut rhs: BigInt) -> Self::Output
	{
		rhs += self;
		rhs
	}
}
impl Add<BigInt> for BigInt
{
	type Output = BigInt;

	fn add(mut self, rhs: BigInt) -> Self::Output
	{
		self += rhs;
		self
	}
}
impl AddAssign<i32> for BigInt
{
	fn add_assign(&mut self, rhs: i32)
	{
		match (self.signum, rhs.signum()) {
			(_, 0) => { },
			(0, s) => {
				self.signum = s;
				self.magnitude.set_to(rhs.abs() as u32);
			},
			(1, 1) | (-1, -1) => {
				self.magnitude += rhs.abs() as u32;
			},
			(-1, 1) | (1, -1) => {
				let o = rhs.abs() as u32;
				if let Some(m) = self.magnitude.as_u32() {
					match m.cmp(&o) {
						Ordering::Greater => self.magnitude.set_to(m - o),
						Ordering::Equal => { self.signum = 0; self.magnitude.set_to(0); },
						Ordering::Less => { self.signum *= -1; self.magnitude.set_to(o - m); },
					}
				}
				else {
					self.magnitude -= o;
				}
			},
			_ => unreachable!(),
		}
	}
}
impl AddAssign<BigInt> for BigInt
{
	fn add_assign(&mut self, mut rhs: BigInt)
	{
		match (self.signum, rhs.signum) {
			(_, 0) => { },
			(0, _) => {
				self.signum = rhs.signum;
				self.magnitude = rhs.magnitude;
			},
			(1, 1) | (-1, -1) => {
				self.magnitude += rhs.magnitude;
			},
			(-1, 1) | (1, -1) => {
				match self.magnitude.cmp(&rhs.magnitude) {
					Ordering::Greater => self.magnitude -= rhs.magnitude,
					Ordering::Equal => { self.signum = 0; self.magnitude.set_to(0); },
					Ordering::Less => {
						self.signum *= -1;
						::std::mem::swap(&mut self.magnitude, &mut rhs.magnitude);
						self.magnitude -= rhs.magnitude;
					}
				}
			},
			_ => unreachable!(),
		}
	}
}
impl<'a> AddAssign<&'a BigInt> for BigInt
{
	fn add_assign(&mut self, rhs: &BigInt)
	{
		match (self.signum, rhs.signum) {
			(_, 0) => { },
			(0, _) => {
				self.signum = rhs.signum;
				self.magnitude.clone_from(&rhs.magnitude);
			},
			(1, 1) | (-1, -1) => {
				self.magnitude += &rhs.magnitude;
			},
			(-1, 1) | (1, -1) => {
				match self.magnitude.cmp(&rhs.magnitude) {
					Ordering::Greater => self.magnitude -= &rhs.magnitude,
					Ordering::Equal => { self.signum = 0; self.magnitude.set_to(0); },
					Ordering::Less => {
						self.signum *= -1;
						let mut new_buf = rhs.magnitude.clone();
						::std::mem::swap(&mut self.magnitude, &mut new_buf);
						self.magnitude -= new_buf;
					}
				}
			},
			_ => unreachable!(),
		}
	}
}

impl Sub<BigInt> for BigInt
{
	type Output = BigInt;

	fn sub(mut self, mut rhs: BigInt) -> Self::Output
	{
		rhs.signum *= -1;
		self += rhs;
		self
	}
}

impl Mul<i32> for BigInt
{
	type Output = BigInt;

	fn mul(mut self, rhs: i32) -> Self::Output
	{
		self *= rhs;
		self
	}
}
impl Mul<BigInt> for i32
{
	type Output = BigInt;

	fn mul(self, mut rhs: BigInt) -> Self::Output
	{
		rhs *= self;
		rhs
	}
}
impl Mul<BigInt> for BigInt
{
	type Output = BigInt;

	fn mul(mut self, rhs: BigInt) -> Self::Output
	{
		self *= rhs;
		self
	}
}
impl MulAssign<i32> for BigInt
{
	fn mul_assign(&mut self, rhs: i32)
	{
		self.signum *= rhs.signum();
		self.magnitude *= rhs.abs() as u32;
	}
}
impl MulAssign<BigInt> for BigInt
{
	fn mul_assign(&mut self, rhs: BigInt)
	{
		self.signum *= rhs.signum;
		self.magnitude *= rhs.magnitude;
	}
}

impl Div<i32> for BigInt
{
	type Output = BigInt;

	fn div(mut self, rhs: i32) -> Self::Output
	{
		self /= rhs;
		self
	}
}
impl DivAssign<i32> for BigInt
{
	fn div_assign(&mut self, rhs: i32)
	{
		self.signum /= rhs.signum();
		self.magnitude /= rhs.abs() as u32;
	}
}

impl PartialOrd for BigInt
{
	fn partial_cmp(&self, rhs: &BigInt) -> Option<Ordering>
	{
		Some(self.cmp(rhs))
	}
}
impl Ord for BigInt
{
	fn cmp(&self, rhs: &BigInt) -> Ordering
	{
		self.signum
			.cmp(&rhs.signum)
			.then_with(|| {
				if self.signum < 0 { self.magnitude.cmp(&rhs.magnitude).reverse() }
				else { self.magnitude.cmp(&rhs.magnitude) }
			})
	}
}

impl fmt::Display for BigInt
{
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
	{
		if self.signum == -1 {
			write!(f, "-")?;
		}

		self.magnitude.fmt(f)
	}
}
