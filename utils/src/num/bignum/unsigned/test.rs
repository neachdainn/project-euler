use super::*;

/// The key test.
///
/// All of the other tests will depend on the ability to go to and from byte
/// representations. This is probably the best way to make these tests not
/// depend on the internal representations of the number.
#[test]
fn to_from_bytes()
{
	let num = BigUInt::from_bytes(&consts::A);
	assert_eq!(num, BigUInt { inner: vec![0x001a6461,0x649d9065, 0x7834586c, 0x21].into() }, "incorrect from bytes");
	assert_eq!(num.as_bytes(), consts::A, "incorrect to bytes");

	let num = BigUInt::from_bytes(&[0]);
	assert_eq!(num, BigUInt { inner: vec![0x00000000].into() }, "incorrect from zero byte");
	assert_eq!(num.as_bytes(), &[0], "incorrect into zero byte");

	let num = BigUInt::from_bytes(&[]);
	assert_eq!(num, BigUInt { inner: vec![0x00000000].into() }, "incorrect from empty byte");
	assert_eq!(num.as_bytes(), &[0], "incorrect into empty byte");
}

#[test]
fn super_short_addition()
{
	let mut num = BigUInt { inner: vec![13].into() };
	num += 12u32;
	assert_eq!(num, BigUInt { inner: vec![25].into() }, "incorrect addition");
}

#[test]
fn short_addition()
{
	let mut num = BigUInt::from_bytes(&consts::A);
	num += consts::Z;

	assert_eq!(num.as_bytes(), consts::A_PLUS_Z, "incorrect addition");
}

#[test]
fn addition()
{
	let a = BigUInt::from_bytes(&consts::A);
	let b = BigUInt::from_bytes(&consts::B);
	let c = a + b;

	assert_eq!(c.as_bytes(), consts::A_PLUS_B, "incorrect addition");
}

#[test]
fn super_short_subtraction()
{
	let mut num = BigUInt { inner: vec![25].into() };
	num -= 12u32;
	assert_eq!(num, BigUInt { inner: vec![13].into() }, "incorrect subtraction");
}

#[test]
fn short_subtraction()
{
	let mut num = BigUInt::from_bytes(&consts::A_PLUS_Z);
	num -= consts::Z;

	assert_eq!(num.as_bytes(), consts::A, "incorrect subtraction");
}

#[test]
fn subtraction()
{
	let c = BigUInt::from_bytes(&consts::A_PLUS_B);
	let b = BigUInt::from_bytes(&consts::B);
	let a = c - b;

	assert_eq!(a.as_bytes(), consts::A, "incorrect subtractions");
}

#[test]
fn subtraction_reverse_ref()
{
	let c  = BigUInt::from_bytes(&consts::A_PLUS_B);
	let b = BigUInt::from_bytes(&consts::B);
	let a = &c - b;

	assert_eq!(a.as_bytes(), consts::A, "incorrect subtractions");
}

#[test]
#[should_panic]
fn subtraction_fail_size()
{
	// We're testing behavior here of implementation details. I.e, we want to
	// fail because the size isn't correct.
	let a = BigUInt { inner: vec![1, 2, 3].into() };
	let b = BigUInt { inner: vec![1, 2, 3, 4].into() };

	let _ = a - b;
}

#[test]
#[should_panic]
fn subtraction_fail_borrow()
{
	// Here we want to fail because we can't successfully borrow a number. We
	// don't care about the digit size, really.
	let a = BigUInt { inner: vec![1, 2, 3, 3].into() };
	let b = BigUInt { inner: vec![1, 2, 3, 4].into() };
	a - b;
}

#[test]
#[should_panic]
fn short_subtraction_borrow()
{
	BigUInt { inner: vec![21].into() } - 62u32;
}

#[test]
fn super_short_multiplication()
{
	let mut num = BigUInt { inner: vec![52].into() };
	num *= 8u32;

	assert_eq!(num, BigUInt { inner: vec![8 * 52].into() }, "incorrect multiplication");
}

#[test]
fn short_multiplication()
{
	let mut num = BigUInt::from_bytes(&consts::A);
	num *= consts::Z;

	assert_eq!(num.as_bytes(), consts::A_TIMES_Z, "incorrect_multiplication");
}

#[test]
fn multiplication()
{
	let a = BigUInt::from_bytes(&consts::A);
	let b = BigUInt::from_bytes(&consts::B);
	let c = a * b;

	assert_eq!(c.as_bytes(), consts::A_TIMES_B, "incorrect multiplication");
}

#[test]
fn super_short_division()
{
	let mut num = BigUInt::new(39);
	let rem = num.short_division(6);

	assert_eq!(num, BigUInt { inner: vec![6].into() }, "incorrect division");
	assert_eq!(rem, 3, "incorrect remainder")
}

#[test]
fn short_division()
{
	let mut num = BigUInt::from_bytes(&consts::A_TIMES_Z);
	let target_rem = consts::Z / 2;

	num += target_rem;
	let rem = num.short_division(consts::Z);

	assert_eq!(num.as_bytes(), consts::A, "incorrect division");
	assert_eq!(rem, target_rem, "incorrect remainder");
}

#[test]
fn pow()
{
	let res = BigUInt::new(7).pow(36);
	assert_eq!(res.as_bytes(), consts::A, "incorrect power");
}

#[test]
fn log2_small_values()
{
	// You have to get fairly large before the `f64` and the `BigUInt` log₂
	// approximations differ.
	let root = 1880708736;
	assert_eq!(BigUInt::new(root).log2(), (root as f64).log2(), "bad log₂ conversion");

	let root = 3644172430;
	assert_eq!(BigUInt::new(root).log2(), (root as f64).log2(), "bad log₂ conversion");

	let root = 30440;
	assert_eq!(BigUInt::new(root).log2(), (root as f64).log2(), "bad log₂ conversion");

	let root = 8;
	assert_eq!(BigUInt::new(root).log2(), (root as f64).log2(), "bad log₂ conversion");
}

#[test]
fn floating_point_zero()
{
	let num = BigUInt::new(0);
	assert_eq!(num.as_f64(), 0.0, "bad floating point approximation");
}

#[test]
fn floating_point_small()
{
	// The number is small, so it can perfectly fit in an `f64`.
	let num = BigUInt::new(5123245);
	assert_eq!(num.as_f64(), 5123245.0, "bad floating point approximation");
}

#[test]
fn floating_point_large()
{
	let num = BigUInt::from_bytes(&consts::A);
	assert_eq!(num.as_f64(), 2.65173084585965359449053226598E30, "bad floating point approximation");

	let num = BigUInt::from_bytes(&consts::B);
	assert_eq!(num.as_f64(), 4.239115827521620352042008576E28, "bad floating point approximation");
}

#[test]
fn floating_point_inf()
{
	let mut bytes = [0; 129];
	bytes[128] = 0xFF;

	let num = BigUInt::from_bytes(&bytes);
	assert_eq!(num.as_f64(), ::std::f64::INFINITY, "did not convert to infinity");

	let mut bytes = [0; 128];
	bytes[127] = 0xFF;

	let num = BigUInt::from_bytes(&bytes);
	let fp = num.as_f64();
	assert!(fp > 0.0 && fp.is_finite(), "incorrect floating point conversion");
}

#[test]
fn clone_from()
{
	let from = BigUInt::from_bytes(&consts::B);
	let mut to = BigUInt::from_bytes(&consts::A);

	to.clone_from(&from);
	assert_eq!(from, to);
}

#[test]
fn digits()
{
	let num = BigUInt::from_bytes(&consts::A);
	assert_eq!(num.into_digits(10).collect::<Vec<_>>(), consts::A_BASE_10.to_vec());
}

#[test]
fn from_digits()
{
	let digits = consts::A_BASE_10.to_vec();
	assert_eq!(BigUInt::from_digits(digits, 10).unwrap(), BigUInt::from_bytes(&consts::A));
}

#[test]
fn format()
{
	let num = BigUInt::from_bytes(&consts::A);
	assert_eq!(consts::A_STR, format!("{}", num));
}

/// Predefined large numbers
mod consts
{
	/// 7³⁶
	pub const A: [u8; 13] = [0x61, 0x64, 0x1a, 0x00, 0x65, 0x90, 0x9d, 0x64, 0x6c, 0x58, 0x34, 0x78, 0x21];

	/// 9³⁰
	pub const B: [u8; 12] = [0xb1, 0xf5, 0xe1, 0x92, 0xfe, 0xa7, 0xed, 0xce, 0xee, 0x24, 0xf9, 0x88];

	/// 2 * (7³⁶ mod 2³²)
	pub const Z: u32 = 0x34c8c2;

	/// 7³⁶ in base 10
	pub const A_BASE_10: [u32; 31] = [1, 0, 6, 1, 8, 3, 3, 2, 0, 9, 7, 7, 1, 7, 4, 3, 5, 6, 9, 5, 8, 5, 4, 8, 0, 3, 7, 1, 5, 6, 2];

	/// 7³⁶ in base 10 as a string
	pub const A_STR: &'static str = "2651730845859653471779023381601";

	/// 7³⁶ + 9³⁰
	pub const A_PLUS_B: [u8; 13] = [0x12, 0x5a, 0xfc, 0x92, 0x63, 0x38, 0x8b, 0x33, 0x5b, 0x7d, 0x2d, 0x01, 0x22];

	/// 7³⁶ + 2 * (7³⁶ mod 2³²)
	pub const A_PLUS_Z: [u8; 13] = [0x23, 0x2d, 0x4f, 0x00, 0x65, 0x90, 0x9d, 0x64, 0x6c, 0x58, 0x34, 0x78, 0x21];

	/// 7³⁶ * 9³⁰
	pub const A_TIMES_B: [u8; 25] = [0x11, 0x3c, 0x91, 0xdf, 0x9d, 0x82, 0x6d, 0x2c, 0xbc, 0x9c, 0x94, 0x47, 0xc5, 0x91, 0x1c, 0x80, 0x32, 0x63, 0xc7, 0xc1, 0x03, 0x8e, 0x6e, 0xe8, 0x11];

	/// 7³⁶ * (2 * (7³⁶ mod 2³²))
	pub const A_TIMES_Z: [u8; 16] = [0x82, 0xd9, 0x1f, 0x16, 0xfb, 0x59, 0xba, 0xac, 0xc0, 0x20, 0xc4, 0xf2, 0xe7, 0xa9, 0xe6, 0x06];
}
