use std::cmp::{self, Ordering};
use std::ops::*;
use std::{fmt, mem, ptr, slice};

#[cfg(test)] mod test;

use smallvec::SmallVec;

use itertools::IntoDoubleEndedIterator;
use num::{Digits, One, FromDigitError};

/// Underlying type used for digits.
///
/// Unfortunately, this shouldn't be a `u64` at this time. Right now, it does
/// not seem like there are any mainstream processors that natively support
/// 128bit arithmetic operations. This means that some operations (i.e.,
/// division) are over 100x slower than their `u64` counterparts.
type Digit = u32;

/// Type used to account for overflow in division and multiplication
/// operations.
///
/// This type should be at least twice the size of the `Digit` type. If it is
/// not exactly twice the size, it is likely that something inefficient is
/// happening.
type SuperDigit = u64;

/// Size in bytes before the `SmallVec` should allocate.
const NUM_SMALL_VEC_BYTES: usize = 32;

/// Array type used for the `SmallVec`.
type VecBacking = [Digit; NUM_SMALL_VEC_BYTES / mem::size_of::<Digit>()];

/// Arbitrarily sized unsigned integer.
#[derive(PartialEq, Eq, Debug, Hash)]
pub struct BigUInt
{
	inner: SmallVec<VecBacking>,
}
impl BigUInt
{
	pub fn new(val: u32) -> Self
	{
		let mut inner = SmallVec::new();
		inner.push(Digit::from(val));

		BigUInt { inner }
	}

	#[cfg(target_endian = "little")]
	pub fn from_bytes(bytes: &[u8]) -> Self
	{
		// I do not imagine myself ever running any of this code on a machine
		// that isn't little endian. However, I would like to know if I ever
		// try. The Git history will contain a record of how to do this in an
		// endian agnostic way.

		// Calculate the number of digits we need and allocate memory for them
		let num_elements = ((bytes.len().saturating_sub(1)) / mem::size_of::<Digit>()) + 1;
		let mut inner = SmallVec::with_capacity(num_elements);

		// We know we're little endian, so it is valid to just copy the
		// elements over. First we make sure the memory is all zero so that
		// anything not overwritten can be ignored.
		let cap = inner.capacity();
		let ptr = inner.as_mut_ptr();

		assert!(cap * mem::size_of::<Digit>() >= bytes.len(), "size mismatch");

		unsafe {
			// It is important to note that the copy takes `ptr` as a pointer
			// to `u8`. This is because we don't necessarily want to totally
			// fill the buffer.
			ptr::write_bytes(ptr, 0x00, cap);
			ptr::copy_nonoverlapping(bytes.as_ptr(), ptr as *mut u8, bytes.len());
			inner.set_len(num_elements);
		}

		BigUInt { inner }
	}

	#[cfg(target_endian = "little")]
	pub fn as_bytes(&self) -> &[u8]
	{
		// Git history shows how to do this in an endian agnostic way.

		let leading_zeros = self.inner.last().unwrap().leading_zeros() as usize;
		let total_bits = 8 * mem::size_of::<Digit>() * self.inner.len();
		let num_bytes = ((total_bits - leading_zeros).saturating_sub(1) / 8) + 1;

		unsafe { slice::from_raw_parts(self.inner.as_ptr() as *const u8, num_bytes) }
	}

	pub fn is_zero(&self) -> bool
	{
		self.inner.len() == 1 && self.inner[0] == 0
	}

	pub fn is_one(&self) -> bool
	{
		self.inner.len() == 1 && self.inner[0] == 1
	}

	/// Set this number to the specified value.
	pub fn set_to(&mut self, val: u32)
	{
		self.inner[0] = Digit::from(val);
		self.inner.truncate(1);
	}

	pub fn pow(mut self, mut exp: u32) -> BigUInt
	{
		// Check for the number that will allow us to not do any work.
		if self.is_zero() {
			return BigUInt::new(0);
		}

		if exp == 0 {
			return BigUInt::new(1);
		}

		if exp == 1 {
			return self.clone();
		}

		// We may be able to avoid several clones, so try that.
		while exp & 1 == 0 {
			self *= self.clone();
			exp >>= 1;
		}

		if exp == 1 { return self }

		// We weren't so lucky.
		let mut result = self.clone();
		while exp > 0 {
			exp >>= 1;
			self *= self.clone();

			if exp & 1 == 1 {
				result *= &self;
			}
		}

		result
	}

	/// Approximates log₂ of this number.
	///
	/// This function will continue to give a relatively accurate answer long
	/// after `as_f64` starts returning infinity.
	pub fn log2(&self) -> f64
	{
		if self.is_zero() {
			return ::std::f64::NAN;
		}

		// Start by calculating the position of the most significant bit.
		let msb = {
			let leading_zeros = self.inner.last().unwrap().leading_zeros() as usize;
			let total_bits = 8 * mem::size_of::<Digit>() * self.inner.len();
			total_bits - leading_zeros
		};

		// Now we have to manually construct an f64 based on the penultimate
		// bits. The goal here is to create a floating point that is:
		// 1.<msbits_minus_msb>
		// We can use up to 52 bits of the mantissa.
		let num_bits_to_use = cmp::min(52, msb - 1);

		let mut floating_point = 0x3FF0000000000000u64;
		for i in 0..num_bits_to_use {
			let bit = self.get_bit(msb - (i +  1));
			let mask = bit << (52 - i);
			floating_point |= mask;
		}

		(msb - 1) as f64 + f64::from_bits(floating_point).log2()
	}

	pub fn as_f64(&self) -> f64
	{
		// The logic below assumes that we have at least a single bit set.
		if self.is_zero() {
			return 0.0;
		}

		// We're manually constructing a floating point out of the 53 most
		// significant bits. The first thing we need to do is calculate the
		// exponent based on our most signficant bit.
		let msb = {
			let leading_zeros = self.inner.last().unwrap().leading_zeros() as usize;
			let total_bits = 8 * mem::size_of::<Digit>() * self.inner.len();
			total_bits - leading_zeros - 1
		};

		// If the most significant bit is too large, we can just return infinity.
		if msb > 1023 {
			return ::std::f64::INFINITY;
		}

		// We know this number will be positive, so the exponent is just the
		// most significant bit plus 1023.
		let exp: u64 = 1023 + msb as u64;

		// The mantissa is the 52 penultimate most significant bits.
		let mantissa = {
			let mut m: u64 = 0;
			let num_bits_to_use = cmp::min(52, msb);
			for i in 0..num_bits_to_use {
				let bit = self.get_bit(msb - (i + 1));
				let mask = bit << (51 - i);
				m |= mask;
			}

			// Round if needed.
			if msb > 52 {
				m += self.get_bit(msb - 53);
			}

			m
		};

		// Then we put it all together.
		f64::from_bits((exp << 52) | mantissa)
	}

	/// Returns the value of this number as a u32, if possible.
	pub fn as_u32(&self) -> Option<u32>
	{
		// TODO: Should probably be replaced with some form of `TryFrom` once stabilized.
		if self.inner.len() == 1 && self.inner[0] <= Digit::from(u32::max_value()) {
			Some(self.inner[0] as u32)
		} else { None }
	}

	fn get_bit(&self, bit: usize) -> u64
	{
		let digit = bit / (mem::size_of::<Digit>() * 8);
		let bit_in_digit = bit % (mem::size_of::<Digit>() * 8);

		((self.inner[digit] >> bit_in_digit) & 0x01) as u64
	}

	fn propagate_add(&mut self, mut val: Digit, offset: usize)
	{
		if val == 0 { return; }

		if offset >= self.inner.len() {
			self.inner.truncate(offset);
		}

		for digit in self.inner[offset..].iter_mut() {
			let (v, o) = digit.overflowing_add(val);
			*digit = v;

			if o { val = 1 } else { return; }
		}

		if val != 0 {
			self.inner.push(val);
		}
	}

	fn remove_leading_zeros(&mut self)
	{
		let num_leading_zeros = self.inner.iter().rev().take_while(|&&v| v == 0).count();
		let len = cmp::max(self.inner.len() - num_leading_zeros, 1);
		self.inner.truncate(len);
	}

	fn propagate_sub(&mut self, val: Digit, offset: usize)
	{
		// Do the subtraction
		if val != 0 && self.inner[offset] >= val {
			// If the digit in this position is more than the value, we can
			// just subtract it.
			self.inner[offset] -= val;
		}
		else if val != 0 {
			// Assume that we'll be able to borrow from a more significant
			// digit.
			self.inner[offset] = self.inner[offset].wrapping_sub(val);

			// Now, find the more significant digit that we borrowed from.
			// Subtract one from it and set all the zeros between this digit
			// and the most significant to the max value.
			self.inner[offset + 1..].iter_mut()
			    .map(|d| { *d = if *d == 0 { Digit::max_value() } else { *d - 1 }; *d })
			    .filter(|&d| d != Digit::max_value())
			    .next().expect("subtract with overflow");
		}

		self.remove_leading_zeros();
	}

	fn short_division(&mut self, dividend: Digit) -> Digit
	{
		assert!(dividend > 0, "divide by zero");

		if dividend == 1 {
			return 0;
		}

		let dividend = SuperDigit::from(dividend);

		let mut k: Digit = 0;
		for d in self.inner.iter_mut().rev() {
			let u: SuperDigit = (SuperDigit::from(k) << (8 * mem::size_of::<Digit>())) | SuperDigit::from(*d);

			let q = u / dividend;
			let r = u % dividend;
			assert!(q <= SuperDigit::from(Digit::max_value()), "division overflow");

			*d = q as Digit;
			k = r as Digit;
		}

		self.remove_leading_zeros();

		k
	}
}

impl Clone for BigUInt
{
	fn clone(&self) -> Self
	{
		BigUInt {
			inner: self.inner.clone(),
		}
	}

	fn clone_from(&mut self, source: &Self)
	{
		// This type uses SmallVec which doesn't have an efficient
		// `clone_from`, so we have to do it ourselves. Begin with clearing to
		// avoid copying elements that we're just going to drop anyway.
		self.inner.clear();

		// Preallocate any space needed
		if source.inner.len() > self.inner.capacity() {
			self.inner.grow(source.inner.len());
		}

		// Then extend our inner vec by the source's
		unsafe {
			let ptr = self.inner.as_mut_ptr();
			let cap = self.inner.capacity();

			assert!(cap >= source.inner.len());

			let s = slice::from_raw_parts_mut(ptr, source.inner.len());
			s.copy_from_slice(&source.inner.as_slice());
		}

		// Finally, update the length to match the new data
		unsafe { self.inner.set_len(source.inner.len()); }
	}
}

impl Digits for BigUInt
{
	type Elem = u32;
	type Iter = Iter;

	fn to_digits(&self, base: Self::Elem) -> Self::Iter
	{
		Iter { inner: self.clone(), base }
	}

	fn into_digits(self, base: Self::Elem) -> Self::Iter
	{
		Iter { inner: self, base }
	}

	fn from_digits<I>(iter: I, base: Self::Elem) -> Result<BigUInt, FromDigitError>
		where I: IntoDoubleEndedIterator<Item=Self::Elem>
	{
		let mut iter = iter.into_double_ended_iter().rev();

		// Try to guess how large of a vector we will need to store this number
		let iter_len_guess = {
			let (lower, upper) = iter.size_hint();
			upper.unwrap_or(lower)
		};

		let size_guess = {
			let bits = (base as f64).log2() * iter_len_guess as f64;
			(bits / (mem::size_of::<Digit>() as f64 * 8.0)).ceil() as usize
		};

		// Allocate the BigUInt
		let mut big_num = {
			let mut inner = SmallVec::with_capacity(size_guess);
			inner.push(0);

			BigUInt { inner }
		};

		// Iterate over the elements, multiplying by the base each time
		if let Some(d) = iter.next() {
			let mut current = d;
			while let Some(d) = iter.next() {
				if current >= base {
					return Err(FromDigitError::InvalidDigit);
				}

				big_num += current;
				big_num *= base;

				current = d;
			}

			big_num += current;
		}

		Ok(big_num)
	}
}

impl One for BigUInt
{
	fn one() -> Self
	{
		BigUInt::new(1)
	}
}

impl<T> Add<T> for BigUInt
	where T: Into<Digit>
{
	type Output = BigUInt;

	fn add(mut self, rhs: T) -> Self::Output
	{
		self.propagate_add(rhs.into(), 0);
		self
	}
}
impl<T> AddAssign<T> for BigUInt
	where T: Into<Digit>
{
	fn add_assign(&mut self, rhs: T)
	{
		self.propagate_add(rhs.into(), 0);
	}
}
impl Add<BigUInt> for BigUInt
{
	type Output = BigUInt;

	fn add(mut self, rhs: BigUInt) -> Self::Output
	{
		self.add_assign(rhs);
		self
	}
}
impl<'a> Add<&'a BigUInt> for BigUInt
{
	type Output = BigUInt;

	fn add(mut self, rhs: &BigUInt) -> Self::Output
	{
		self.add_assign(rhs);
		self
	}
}
impl AddAssign<BigUInt> for BigUInt
{
	fn add_assign(&mut self, mut rhs: BigUInt)
	{
		// We can try to avoid allocation by taking ownership of whatever
		// vector is larger.
		if self.inner.capacity() < rhs.inner.capacity() {
			::std::mem::swap(&mut self.inner, &mut rhs.inner);
		}

		// Otherwise, ownership doesn't do a whole lot for us.
		self.add_assign(&rhs);
	}
}
impl<'a> AddAssign<&'a BigUInt> for BigUInt
{
	fn add_assign(&mut self, rhs: &BigUInt)
	{
		// Since we're adding, the final length will be either the length of
		// the longest number or one more than that. So we can simplify logic
		// by zero padding our array until it is the same length (or longer)
		// than the other array.
		if self.inner.len() < rhs.inner.len() {
			let len = self.inner.len();
			let rhs_len = rhs.inner.len();
			self.inner.insert_many(len, (0..rhs_len - len).map(|_| 0));
		}

		// We just did the resize, so we know that the rhs is no longer than we
		// are. This means that we can figure out exactly where the overflow
		// needs to go.
		let mut overflow: Digit = 0;
		for (u, &t) in self.inner.iter_mut().zip(rhs.inner.iter()) {
			let (v, o) = {
				let (v0, o0) = u.overflowing_add(t);
				let (v1, o1) = v0.overflowing_add(overflow);

				(v1, o0 || o1)
			};

			*u = v;
			overflow = if o { 1 } else { 0 };
		}

		// Propagate any overflow.
		self.propagate_add(overflow, rhs.inner.len());
	}
}

impl<T> Sub<T> for BigUInt
	where T: Into<Digit>
{
	type Output = BigUInt;

	fn sub(mut self, rhs: T) -> Self::Output
	{
		self.propagate_sub(rhs.into(), 0);
		self
	}
}
impl<T> SubAssign<T> for BigUInt
	where T: Into<Digit>
{
	fn sub_assign(&mut self, rhs: T)
	{
		self.propagate_sub(rhs.into(), 0);
	}
}
impl Sub<BigUInt> for BigUInt
{
	type Output = BigUInt;

	fn sub(mut self, rhs: BigUInt) -> BigUInt
	{
		self.sub_assign(rhs);
		self
	}
}
impl<'a> Sub<&'a BigUInt> for BigUInt
{
	type Output = BigUInt;

	fn sub(mut self, rhs: &BigUInt) -> BigUInt
	{
		self.sub_assign(rhs);
		self
	}
}
impl<'a> Sub<BigUInt> for &'a BigUInt
{
	type Output = BigUInt;

	fn sub(self, mut rhs: BigUInt) -> BigUInt
	{
		assert!(*self >= rhs, "attempt to subtract with overflow");

		// Make sure the rhs is as long as the left, because the below assumes
		// that it does.
		let len = self.inner.len();
		let rhs_len = rhs.inner.len();
		rhs.inner.insert_many(rhs_len, (0..len - rhs_len).map(|_| 0));

		// Do the subtraction assuming that everything will work out
		let end = cmp::min(self.inner.len(), rhs.inner.len());
		let mut borrow = 0;
		for i in 0..end {
			let (v, o) = {
				let (v0, o0) = self.inner[i].overflowing_sub(rhs.inner[i]);
				let (v1, o1) = v0.overflowing_sub(borrow);
				(v1, o0 || o1)
			};

			rhs.inner[i] = v;
			borrow = if o { 1 } else { 0 };
		}

		// Then we make sure that things actually did work out
		rhs.propagate_sub(borrow, end);

		rhs
	}
}
impl SubAssign<BigUInt> for BigUInt
{
	fn sub_assign(&mut self, rhs: BigUInt)
	{
		// The arrays are shrinking here, so having ownership of the
		// right-hand-side literally does nothing for us.
		self.sub_assign(&rhs);
	}
}
impl<'a> SubAssign<&'a BigUInt> for BigUInt
{
	fn sub_assign(&mut self, rhs: &BigUInt)
	{
		assert!(*self >= *rhs, "attempt to subtract with overflow");

		// Do the subtraction assuming that everything will work out
		let mut borrow = 0;
		for i in 0..rhs.inner.len() {
			let (v, o) = {
				let (v0, o0) = self.inner[i].overflowing_sub(rhs.inner[i]);
				let (v1, o1) = v0.overflowing_sub(borrow);
				(v1, o0 || o1)
			};

			self.inner[i] = v;
			borrow = if o { 1 } else { 0 };
		}

		// Then we make sure that things actually did work out
		self.propagate_sub(borrow, rhs.inner.len());
	}
}

impl<T> Mul<T> for BigUInt
	where T: Into<Digit>
{
	type Output = BigUInt;

	fn mul(mut self, rhs: T) -> Self::Output
	{
		self.mul_assign(rhs);
		self
	}
}
impl<T> MulAssign<T> for BigUInt
	where T: Into<Digit>
{
	fn mul_assign(&mut self, rhs: T)
	{
		let rhs = rhs.into();

		// Check if we're zero. Make no modifications if we are.
		if rhs == 1 || self.is_zero() {
			return;
		}

		// Check if the rhs is zero. Reduce our inner vector to zero if it is.
		if rhs == 0 {
			self.inner[0] = 0;
			self.inner.truncate(1);
			return;
		}

		// Writing literal constants is a pain when you alias types.
		let lower_mask = SuperDigit::from(Digit::max_value());
		let shift_amount = mem::size_of::<Digit>() * 8;

		// Otherwise, perform the multiplication and propagate any overflow.
		let rhs = SuperDigit::from(rhs);
		let overflow = self.inner.iter_mut().fold(0, |acc, d| {
			let v: SuperDigit = SuperDigit::from(*d) * rhs + acc;
			*d = (v & lower_mask) as Digit;
			((v >> shift_amount) & lower_mask)
		}) as Digit;

		let len = self.inner.len(); // borrowck
		self.propagate_add(overflow, len);
	}
}
impl Mul<BigUInt> for BigUInt
{
	type Output = BigUInt;

	fn mul(self, rhs: BigUInt) -> BigUInt
	{
		(&self).mul(&rhs)
	}
}
impl<'a> Mul<&'a BigUInt> for BigUInt
{
	type Output = BigUInt;

	fn mul(self, rhs: &BigUInt) -> BigUInt
	{
		(&self).mul(rhs)
	}
}
impl<'a> Mul<BigUInt> for &'a BigUInt
{
	type Output = BigUInt;

	fn mul(self, rhs: BigUInt) -> BigUInt
	{
		self.mul(&rhs)
	}
}
impl<'a, 'b> Mul<&'a BigUInt> for &'b BigUInt
{
	type Output = BigUInt;

	fn mul(self, rhs: &BigUInt) -> BigUInt
	{
		if self.is_zero() || rhs.is_one() {
			return self.clone();
		}

		if rhs.is_zero() {
			let mut inner = SmallVec::new();
			inner.push(0);
			return BigUInt { inner };
		}

		// It's hard to write literal constants when you've aliased the types.
		let lower_mask = SuperDigit::from(Digit::max_value());
		let shift_amount = mem::size_of::<Digit>() * 8;

		// Three major points:
		// 1. This algorithm is from The Art Of Computer Programming.
		// 2. I don't think we can make this constant size, which is why all
		//    the other `Mul` functions just map to this one.
		// 3. Theoretically faster algorithms exist, but this is faster in
		//    practice for "reasonably" sized numbers.
		let u = &self.inner;
		let v = &rhs.inner;
		let m = u.len();

		// M1. [Initialize]
		let mut w: SmallVec<VecBacking> = (0..u.len() + v.len()).map(|_| 0).collect();

		for (j, &v_j) in v.iter().enumerate() {
			// M2. [Zero multiplier?]
			//
			// NOTE: The zero multiplier step is really only effective if the
			// number of zeros is substantial, which is only likely with small
			// bases. We are not using small bases, so it has been removed to
			// speed things up.

			// M3. [Initialize i]
			let mut k: Digit = 0;
			for (i, &u_i) in u.iter().enumerate() {
				// M4. [Multiply and add]
				let t: SuperDigit = SuperDigit::from(u_i) * SuperDigit::from(v_j) + SuperDigit::from(w[i + j]) + SuperDigit::from(k);

				w[i + j] = (t & lower_mask) as Digit;
				k = ((t >> shift_amount) & lower_mask) as Digit;

				// M5. [Loop on i]
			}

			w[j + m] = k;

			// M6. [Loop on j]
		}

		// Algorithm terminates
		let mut res = BigUInt { inner: w };
		res.remove_leading_zeros();

		res
	}
}
impl MulAssign<BigUInt> for BigUInt
{
	fn mul_assign(&mut self, rhs: BigUInt)
	{
		let mut v = &*self * rhs;
		mem::swap(&mut self.inner, &mut v.inner);
	}
}
impl<'a> MulAssign<&'a BigUInt> for BigUInt
{
	fn mul_assign(&mut self, rhs: &BigUInt)
	{
		let mut v = &*self * rhs;
		mem::swap(&mut self.inner, &mut v.inner);
	}
}

impl<T> Div<T> for BigUInt
	where T: Into<Digit>
{
	type Output = BigUInt;

	fn div(mut self, rhs: T) -> Self::Output
	{
		self.div_assign(rhs);
		self
	}
}
impl<T> DivAssign<T> for BigUInt
	where T: Into<Digit>
{
	fn div_assign(&mut self, rhs: T)
	{
		self.short_division(rhs.into());
	}
}
impl PartialOrd for BigUInt
{
	fn partial_cmp(&self, rhs: &BigUInt) -> Option<Ordering>
	{
		Some(self.cmp(rhs))
	}
}
impl Ord for BigUInt
{
	fn cmp(&self, rhs: &BigUInt) -> Ordering
	{
		self.inner.len().cmp(&rhs.inner.len()).then_with(||{
			// They have equal lengths, so see who has the largest most
			// significant digit.
			for (s,r) in self.inner.iter().rev().zip(rhs.inner.iter().rev()) {
				match s.cmp(r) {
					Ordering::Equal => { },
					c => return c,
				}
			}

			Ordering::Equal
		})
	}
}

impl fmt::Display for BigUInt
{
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
	{
		// If we implemented DoubleEndedIterator for Iter, we could potentially
		// just iterate in reverse and write it out. However, we need to
		// allocate a new string anyway in order to use the `pad_integral`
		// function, so we may as well just forward collect.
		let mut digits: Vec<_> = self.to_digits(10).map(|d| (b'0' + d as u8)).collect();
		digits.reverse();

		let formatted = String::from_utf8(digits).expect("invalid digit");
		f.pad_integral(true, "", &formatted)
	}
}

/// An iterator over the digits of a BigUInt.
pub struct Iter
{
	inner: BigUInt,
	base: u32,
}
impl Iterator for Iter
{
	type Item = u32;

	fn next(&mut self) -> Option<Self::Item>
	{
		if self.inner.is_zero() {
			return None;
		}

		Some(self.inner.short_division(self.base.into()) as Self::Item)
	}
}
