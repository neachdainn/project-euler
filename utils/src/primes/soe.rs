use std::marker::PhantomData;
use std::ops::Range;
use std::fmt::Display;
use num::PrimInt;

pub struct SieveOfEratosthenes<T>
{
	bits: Vec<u8>,
	_pd: PhantomData<T>,
}
impl<T> SieveOfEratosthenes<T>
	where T: PrimInt + Display
{
	/// Creates a new Sieve of Eratosthenes that generates primes up to, but
	/// not including, the upper limit.
	pub fn new(upper_limit: T) -> Self
		where Range<T>: Iterator
	{
		// Round to the nearest multiple of sixteen
		let _16 = T::from(16).unwrap();
		let _0 = T::zero();

		let upper_limit = if upper_limit % _16 == _0 { upper_limit }
		                  else { upper_limit + (_16 - (upper_limit % _16)) };
		let size = upper_limit / _16;

		// Build a vector so the bits are all one
		let mut bits: Vec<_> = (T::zero()..size).map(|_| 0xFF).collect();

		// Now we do the sieve itself
		for i in (3..upper_limit.to_usize().unwrap()).filter(|&x| x % 2 == 1) {
			// If this number isn't a prime, we can skip it
			if !get_bit(&bits, (i - 3) / 2) { continue; }

			// Set all multiples of this number to composites
			for c in (2..).map(|x| i * x).filter(|&x| x % 2 == 1).take_while(|&x| T::from(x).map(|x| x < upper_limit).unwrap_or(false)) {
				set_bit(&mut bits, (c - 3) / 2, false);
			}
		}

		// Done
		SieveOfEratosthenes { bits, _pd: PhantomData }
	}

	pub fn is_prime(&self, num: T) -> bool
	{
		assert!(num < self.upper_limit(), "number out of bounds: checking for {} but the limit is {}", num, self.upper_limit());

		let num = num.to_usize().unwrap();
		if num == 2 { true }
		else if num % 2 == 0 || num < 3 { false }
		else { get_bit(&self.bits, (num - 3) / 2) }
	}

	/// Returns true if a number is prime.
	///
	/// This differs from `SieveOfEratosthenes::is_prime` in that it will fall
	/// back to the `primes::is_prime` function if the specified number is out
	/// of the sieve's range rather than panicking.
	pub fn is_prime_safe(&self, num: T) -> bool
	{
		if num < self.upper_limit() {
			self.is_prime(num)
		} else {
			super::is_prime(num.to_u64().unwrap())
		}
	}

	pub fn upper_limit(&self) -> T
	{
		T::from(self.bits.len() * 16).unwrap()
	}

	/// Returns a list of the prime factors of the number.
	///
	/// If the number is too large to determine the prime factors, then the
	/// factors we were able to determine as well as the remainders are
	/// returned.
	pub fn factors(&self, num: T) -> Result<Vec<(T, usize)>, (T, Vec<(T, usize)>)>
		where Range<T>: Iterator<Item=T>
	{
		// This method is blatently modeled after the version in `primal`.
		if num == T::zero() { return Err((T::zero(), Vec::new())) }
		if num == T::one() { return Ok(Vec::new()) }
		if num < self.upper_limit() && self.is_prime(num) { return Ok(vec![(num, 1)]) }

		let mut num = num;

		// Factor out all of the primes we know
		let mut res = Vec::new();
		for p in self.iter() {
			// Count how many times the number is divisble by the prime
			let mut count = 0;
			while num % p == T::zero() {
				count += 1;
				num = num / p;
			}

			// If this prime was a divisor, add it to the list
			if count > 0 {
				res.push((p, count));
			}

			// Check to see if this prime is smaller than the square root of
			// the number. If it isn't, there can be no more prime divisors.
			if let Some(pp) = p.checked_mul(&p) {
				if pp < num {
					continue;
				}
			}

			// This number is smaller than the square of this prime. It has no
			// more divisors.
			break;
		}

		// If the number isn't one, then there are still prime factors. This
		// means that either the number is prime or it has factors larger than
		// we know.
		if num != T::one() {
			// We need to check if it's prime. We already tested to see if it's
			// a prime we know, so we have to check to see if its square root
			// is within our range. If it is, than the number must be prime. If
			// not, we can't be sure.
			let b = self.upper_limit();
			if let Some(bb) = b.checked_mul(&b) {
				if bb < num {
					// This number might have factors that we don't know.
					return Err((num, res));
				}
			}

			// This number has to be prime, even though we don't know this
			// particular prime.
			res.push((num, 1));
		}

		Ok(res)
	}

	pub fn iter(&self) -> Iter<T>
	{
		Iter { inner: self, lower: T::one(), upper: self.upper_limit() }
	}
}

#[inline]
fn get_bit(buf: &[u8], index: usize) -> bool
{
	let byte = index / 8;
	let bit = index % 8;

	buf[byte] & (1 << bit) != 0
}

#[inline]
fn set_bit(buf: &mut [u8], index: usize, val: bool)
{
	let byte = index / 8;
	let bit = index % 8;

	if val {
		// Setting the bit
		buf[byte] |= 1 << bit;
	}
	else {
		// Unsetting the bit
		buf[byte] &= !(1 << bit);
	}
}

pub struct Iter<'a, T>
	where T: 'a
{
	inner: &'a SieveOfEratosthenes<T>,
	lower: T,
	upper: T,
}
impl<'a, T> Iterator for Iter<'a, T>
	where T: PrimInt + Display,
	      Range<T>: Iterator<Item=T>
{
	type Item = T;

	fn next(&mut self) -> Option<Self::Item>
	{
		for n in self.lower..self.upper {
			if self.inner.is_prime(n) {
				self.lower = n + T::one() + (n % (T::one() + T::one()));
				return Some(n);
			}
		}

		None
	}

	fn size_hint(&self) -> (usize, Option<usize>)
	{
		// We could have an exact size by iterating over the sieve buffer and
		// counting the ones but I'm not sure having the exact size will be
		// worth that cost
		(0, (self.upper - self.lower).to_usize())
	}
}
impl<'a, T> DoubleEndedIterator for Iter<'a, T>
	where T: PrimInt + Display,
	      Range<T>: DoubleEndedIterator<Item=T>
{
	fn next_back(&mut self) -> Option<Self::Item>
	{
		for n in (self.lower..self.upper).rev() {
			if self.inner.is_prime(n) {
				self.upper = n;
				return Some(n);
			}
		}

		None
	}
}

#[cfg(test)]
mod test
{
	#[test]
	fn first_11_primes()
	{
		let primes: [u32; 11] = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31];

		// We collect to make sure that primes are actually generated
		let gen_primes: Vec<_> = super::SieveOfEratosthenes::new(32).iter().collect();

		assert_eq!(&primes[..], &gen_primes[..]);
	}

	#[test]
	fn first_11_primes_rev()
	{
		let primes: [u32; 11] = [31, 29, 23, 19, 17, 13, 11, 7, 5, 3, 2];

		// We collect to make sure that primes are actually generated
		let gen_primes: Vec<_> = super::SieveOfEratosthenes::new(32).iter().rev().collect();

		assert_eq!(&primes[..], &gen_primes[..]);
	}

	#[test]
	fn first_11_primes_mixed()
	{
		let gen_primes = {
			let sieve = super::SieveOfEratosthenes::new(32);
			let mut iter = sieve.iter();

			let mut gen_primes = Vec::new();
			loop {
				if let Some(p) = iter.next() {
					gen_primes.push(p);
				} else { break; }

				if let Some(p) = iter.next_back() {
					gen_primes.push(p);
				} else { break; }
			}

			gen_primes
		};

		let primes: [u32; 11] = [2, 31, 3, 29, 5, 23, 7, 19, 11, 17, 13];
		assert_eq!(&primes[..], &gen_primes[..]);
	}

	#[test]
	fn factors()
	{
		let p = super::SieveOfEratosthenes::new(40);
		assert_eq!(p.factors(644), Ok(vec![(2, 2), (7, 1), (23, 1)]));

		// Should work, since it can discover that 43 is prime
		assert_eq!(p.factors(645), Ok(vec![(3, 1), (5, 1), (43, 1)]));

		// Can't factor things that are too large
		assert_eq!(p.factors(157), Ok(vec![(157, 1)]));
		assert_eq!(p.factors(157 * 157), Err((157 * 157, vec![])));
		assert_eq!(p.factors(157 * 157 * 2 * 3 * 3), Err((157 * 157, vec![(2, 1), (3, 2)])));
	}
}
