use num::modpow;

mod soe;
pub use self::soe::SieveOfEratosthenes;

/// The current "best" sieve.
pub type Sieve<T> = SieveOfEratosthenes<T>;

/// Determines if a number is prime.
///
/// This has a few steps.
///
/// 1. Trial division if the number is less than 15,000,000
/// 2. Test against the first 30 primes.
/// 3. Deterministic Miller-Rabin test
pub fn is_prime(n: u64) -> bool
{
	const NUM_PRIMES: usize = 30;
	const FIRST_PRIMES: &'static [u64; NUM_PRIMES] =
		&[2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53,
		  59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113];

	if n <= FIRST_PRIMES[NUM_PRIMES - 1] * FIRST_PRIMES[NUM_PRIMES - 1] || n > 15_000_000 {
		if n < 2 { return false; }
		for &p in FIRST_PRIMES {
			if n == p { return true; }
			if n % p == 0 { return false; }
		}

		rabin_miller(n)
	} else {
		return trial_division(n);
	}
}

/// Determine if a number is prime via trial division.
fn trial_division(n: u64) -> bool
{
	if n < 2 { return false; }
	if n == 2 { return true; }
	if n % 2 == 0 { return false; }
	if n < 9 { return true; }
	if n % 3 == 0 { return false; }

	let potentials = (0..).map(|x| 5 + x * 6)
	                      .take_while(|&x: &u64| x.checked_mul(x)
	                                              .map(|xx| xx <= n)
	                                              .unwrap_or(false));

	for d in potentials {
		if n % d == 0 { return false; }
		if n % (d + 2) == 0 { return false; }
	}

	true
}

/// Determines if a number is prime via deterministic Rabin-Miller.
fn rabin_miller(n: u64) -> bool
{
	if n < 2 { return false; }

	let (s, d) = {
		let mut d = n - 1;
		let mut s = 0;

		while d % 2 == 0 {
			d /= 2;
			s += 1;
		}

		(s, d)
	};

	let a: &[u64] = if n < 2_047                          { &[2] }
	                else if n < 1_373_653                 { &[2, 3] }
	                else if n < 9_080_191                 { &[31, 73] }
	                else if n < 25_326_001                { &[2, 3, 5] }
	                else if n < 3_215_031_751             { &[2, 3, 5, 7] }
	                else if n < 4_759_123_141             { &[2, 7, 61] }
	                else if n < 1_122_004_669_633         { &[2, 13, 23, 1_662_803] }
	                else if n < 2_152_302_898_747         { &[2, 3, 5, 7, 11] }
	                else if n < 3_474_749_660_383         { &[2, 3, 5, 7, 11, 13] }
	                else if n < 341_550_071_728_321       { &[2, 3, 5, 7, 11, 13, 17] }
	                else if n < 3_825_123_056_546_413_051 { &[2, 3, 5, 7, 11, 13, 17, 19, 23] }
	                else { &[2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37] };

	for &a in a {
		if modpow(a, d, n) != 1 && (0..s).all(|r| modpow(a, 2u64.pow(r) * d, n) != n - 1) {
			return false;
		}
	}

	true
}

#[cfg(test)]
mod test
{
	use super::*;

	#[test]
	fn count_primes_low()
	{
		// π(200,000) and π(150,000) = 4136
		let cnt = (150_000..200_000).filter(|&x| is_prime(x)).count();
		assert_eq!(cnt, 4136, "incorrect number of primes");
	}

	#[test]
	fn count_primes_high()
	{
		// π(20,000,000) - π(15,000,000) = 299,903
		let cnt = (15_000_000..20_000_000).filter(|&x| is_prime(x)).count();
		assert_eq!(cnt, 299_903, "incorrect number of primes");
	}
}
