use std::cmp::Ord;

pub struct LexicographicPerm<T>
{
	shown_first: bool,
	items: Vec<T>,
}
impl<T> LexicographicPerm<T>
	where T: Ord
{
	pub fn new(items: Vec<T>) -> Self
	{
		let mut items = items;

		// Sort in reverse to get the correct first permutation
		items.sort_by(|a, b| b.cmp(a));

		LexicographicPerm { shown_first: false,  items }
	}

	/// Return the next permutation of the elements.
	///
	/// This cannot be an iterator since Rust does not support streaming
	/// iterators.
	pub fn next(&mut self) -> Option<&Vec<T>>
	{
		if !self.shown_first {
			self.shown_first = true;
			return Some(&self.items);
		}

		// 1. Find the length of the monotonically increasing run from the
		// start. If the whole thing is monotonically increasing, we're done.
		let end_of_run = match self.items.windows(2).position(|w| w[0] > w[1]) {
			Some(e) => e + 1,
			None => return None,
		};

		// 2. Find the smallest item in the run that is larger than the run
		// breaker. This is guaranteed to exist.
		let to_swap = self.items[0..end_of_run].iter()
		                                       .enumerate()
		                                       .filter(|&(_, s)| *s > self.items[end_of_run])
		                                       .min_by_key(|&(_, s)| s)
		                                       .map(|(e, _)| e)
		                                       .unwrap();

		// 3. Swap the two elements
		self.items.swap(end_of_run, to_swap);

		// 4. Everything in the starting run needs to be reversed.
		self.items[0..end_of_run].reverse();

		// 5. Return the result
		Some(&self.items)
	}

	/// Returns the underlying vector.
	///
	/// This is mostly to enable Project Euler optimizations, not for any
	/// functional reason.
	///
	/// This "streaming iterator" thing is becoming painful.
	pub fn take(self) -> Vec<T>
	{
		self.items
	}
}

#[cfg(test)]
mod test
{
	#[test]
	fn perm_long()
	{
		let mut perms = super::LexicographicPerm::new(vec![0, 3, 3, 5, 2, 1, 0]);
		assert_eq!(perms.next().cloned(), Some(vec![5, 3, 3, 2, 1, 0, 0]));
		assert_eq!(perms.next().cloned(), Some(vec![3, 5, 3, 2, 1, 0, 0]));
	}

	#[test]
	fn perm_complete()
	{
		let mut perms = super::LexicographicPerm::new(vec![3, 2, 1]);
		assert_eq!(perms.next().cloned(), Some(vec![3, 2, 1]));
		assert_eq!(perms.next().cloned(), Some(vec![2, 3, 1]));
		assert_eq!(perms.next().cloned(), Some(vec![3, 1, 2]));
		assert_eq!(perms.next().cloned(), Some(vec![1, 3, 2]));
		assert_eq!(perms.next().cloned(), Some(vec![2, 1, 3]));
		assert_eq!(perms.next().cloned(), Some(vec![1, 2, 3]));
		assert_eq!(perms.next(), None);
	}
}
