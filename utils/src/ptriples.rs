use std::cmp::Ordering;
use std::collections::BinaryHeap;
use num::PrimInt;

/// Iterator over the primitive Pythagorean triples. They will be ordered from
/// the smallest sum to the largest.
pub struct PythagoreanTriples<T>
{
	heap: BinaryHeap<GenSet<T>>,
}
impl<T> PythagoreanTriples<T>
	where T: PrimInt
{
	pub fn new() -> Self
	{
		let mut heap = BinaryHeap::new();
		heap.push(GenSet::new(T::from(3).unwrap(), T::one()));

		PythagoreanTriples { heap }
	}
}
impl<T> Iterator for PythagoreanTriples<T>
	where T: PrimInt
{
	type Item = (T, T, T);

	fn next(&mut self) -> Option<Self::Item>
	{
		// Get the first item
		let item = self.heap.pop();

		item.map(|gs| {
			let GenSet { u, v, a, b, c } = gs;
			let two = T::one() + T::one();
			self.heap.push(GenSet::new(two * u - v, v));
			self.heap.push(GenSet::new(two * u + v, v));
			self.heap.push(GenSet::new(u + two * v, u));

			(a, b, c)
		})
	}
}

#[derive(PartialEq, Eq)]
struct GenSet<T>
{
	u: T,
	v: T,
	a: T,
	b: T,
	c: T,
}
impl<T> GenSet<T>
	where T: PrimInt
{
	fn new(u: T, v: T) -> Self
	{
		let two = T::one() + T::one();
		GenSet { u, v, a: u * v, b: (u*u - v*v)/two, c: (u*u + v*v)/two }
	}
}
impl<T> Ord for GenSet<T>
	where T: PrimInt
{
	fn cmp(&self, other: &GenSet<T>) -> Ordering
	{
		let a = self.a + self.b + self.c;
		let b = other.a + other.b + other.c;

		// Compare them in reverse to do a min-ordering
		b.cmp(&a)
	}
}
impl<T> PartialOrd for GenSet<T>
	where T: PrimInt
{
	fn partial_cmp(&self, other: &GenSet<T>) -> Option<Ordering>
	{
		Some(self.cmp(other))
	}
}


#[cfg(test)]
mod test
{
	use super::*;

	#[test]
	fn valid_triples()
	{
		PythagoreanTriples::<u32>::new().take(10).for_each(|(a, b, c)| {
			assert_eq!(a * a + b * b, c * c);
		})
	}
}
