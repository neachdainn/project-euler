use std::mem;
use std::cmp;
use std::hash::Hash;
use std::cell::UnsafeCell;
use fnv::FnvHashMap;

use num::PrimInt;

const LOWER_CACHE_SIZE: usize = 2097152; // 2MiB

pub struct Memoizer<A, R>
{
	lower_cache: UnsafeCell<Vec<Option<R>>>,
	upper_cache: UnsafeCell<FnvHashMap<A, R>>,
	func: Box<Fn(&Memoizer<A, R>, A) -> R>,
}
impl<A, R> Memoizer<A, R>
	where A: PrimInt + Hash,
	      R: Clone,
{
	pub fn new<F>(func: F) -> Self
		where F: Fn(&Self, A) -> R + 'static
	{
		// This min comparison is an artifact from when I tried to make this
		// generic over the primitives. I've left it just in case I want to try
		// it again.
		let count = cmp::min((LOWER_CACHE_SIZE / mem::size_of::<Option<R>>()).next_power_of_two(), A::max_value().to_usize().unwrap());
		let lower_cache = UnsafeCell::new(vec![None; count]);
		let upper_cache = UnsafeCell::new(FnvHashMap::default());

		Memoizer { lower_cache, upper_cache, func: Box::new(func) }
	}

	pub fn call(&self, arg: A) -> R
	{
		unsafe {
			if (arg.to_usize().unwrap()) < (*self.lower_cache.get()).len() {
				self.call_lower(arg)
			} else { self.call_upper(arg) }
		}
	}

	unsafe fn call_lower(&self, arg: A) -> R
	{
		// If it is in the lower cache, just return it.
		if let &Some(ref val) = (*self.lower_cache.get()).get_unchecked(arg.to_usize().unwrap()) {
			return val.clone();
		}

		// Otherwise calculate it and put it in the lower cache
		let val = (self.func)(self, arg);
		*(*self.lower_cache.get()).get_unchecked_mut(arg.to_usize().unwrap()) = Some(val.clone());
		val
	}

	unsafe fn call_upper(&self, arg: A) -> R
	{
		// Return it if it is in the cache
		if let Some(val) = (*self.upper_cache.get()).get(&arg).cloned() {
			return val;
		}

		// Otherwise calculate it.
		let val = (self.func)(self, arg);
		(*self.upper_cache.get()).insert(arg, val.clone());
		val
	}
}

#[cfg(test)]
mod test
{
	#[test]
	fn fib()
	{
		let memoizer = super::Memoizer::new(|m, n| {
			if n < 2 { 1 }
			else { m.call(n - 1) + m.call(n - 2) }
		});

		assert_eq!(memoizer.call(5), 8);
	}
}
