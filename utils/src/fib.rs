use std::mem;
use std::ops::AddAssign;
use num_crate::One;

/// Iterator for the Fibonacci sequence.
pub struct FibonacciSequence<T>
{
	a: T,
	b: T,
}
impl<T> FibonacciSequence<T>
	where T: One
{
	pub fn new() -> Self
	{
		FibonacciSequence {
			a: T::one(),
			b: T::one(),
		}
	}
}
impl<T> Iterator for FibonacciSequence<T>
	where T: AddAssign<T> + Clone
{
	type Item = T;

	fn next(&mut self) -> Option<Self::Item>
	{
		// Using `AddAssign` instead of `Add` potentially avoids an allocation.
		let mut out = self.a.clone();
		out += self.b.clone();

		mem::swap(&mut out, &mut self.a);
		mem::swap(&mut self.a, &mut self.b);

		Some(out)
	}
}

#[cfg(test)]
mod test
{
	#[test]
	fn first_7_u32()
	{
		let mut fib: super::FibonacciSequence<u32> = super::FibonacciSequence::new();
		assert_eq!(fib.next(), Some(1));
		assert_eq!(fib.next(), Some(1));
		assert_eq!(fib.next(), Some(2));
		assert_eq!(fib.next(), Some(3));
		assert_eq!(fib.next(), Some(5));
		assert_eq!(fib.next(), Some(8));
		assert_eq!(fib.next(), Some(13));
	}
}
