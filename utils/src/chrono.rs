#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub struct Date
{
	year: u32,
	month: Month,
	day: u8,
}
impl Date
{
	pub fn new(year: u32, month: Month, day: u8) -> Self
	{
		assert!(year != 0, "The year 0AD does not exist");
		assert!(day <= month.num_days(year) && day != 0, "Invalid day of month: {}", day);

		Date { year, month, day }
	}

	pub fn yesterday(&self) -> Self
	{
		assert!(*self != Date { year: 1, month: Month::January, day: 1 }, "There is no representable yesterday");

		if self.day == 1 {
			let month = self.month.prev_month();
			let year = if month == Month::December { self.year - 1 } else { self.year };
			let day = month.num_days(year);

			Date { year, month, day }
		} else {
			Date { year: self.year, month: self.month, day: self.day - 1 }
		}
	}

	pub fn tomorrow(&self) -> Self
	{
		if self.day == self.month.num_days(self.year) {
			let month = self.month.next_month();
			let year = if month == Month::January { self.year + 1 } else { self.year };
			let day = 1;

			Date { year, month, day }
		} else {
			Date { year: self.year, month: self.month, day: self.day + 1 }
		}
	}

	pub fn day(&self) -> u8
	{
		self.day
	}

	pub fn month(&self) -> Month
	{
		self.month
	}

	pub fn year(&self) -> u32
	{
		self.year
	}

	pub fn day_of_week(&self) -> DayOfWeek
	{
		// Schwerdtfeger's method
		let (c, g) = match self.month {
			Month::January | Month::February => {
				let c = (self.year - 1) / 100;
				(c, self.year - 1 - 100 * c)
			},
			_ => {
				let c = self.year / 100;
				(c, self.year - 100 * c)
			}
		};

		let e = match self.month {
			Month::January | Month::May => 0,
			Month::February | Month::June => 3,
			Month::March | Month::November => 2,
			Month::April | Month::July => 5,
			Month::August => 1,
			Month::September | Month::December => 4,
			Month::October => 6,
		};

		let f = match c % 4 {
			0 => 0,
			1 => 5,
			2 => 3,
			3 => 1,
			_ => unreachable!(),
		};

		// Now we can determine the index of the day
		let w = (self.day as u32 + e + f + g + (g / 4)) % 7;
		match w {
			0 => DayOfWeek::Sunday,
			1 => DayOfWeek::Monday,
			2 => DayOfWeek::Tuesday,
			3 => DayOfWeek::Wednesday,
			4 => DayOfWeek::Thursday,
			5 => DayOfWeek::Friday,
			6 => DayOfWeek::Saturday,
			_ => unreachable!(),
		}
	}
}

#[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
pub enum Month
{
	January,
	February,
	March,
	April,
	May,
	June,
	July,
	August,
	September,
	October,
	November,
	December
}
impl Month
{
	pub fn num_days(&self, year: u32) -> u8
	{
		assert!(year != 0, "The year 0AD does not exist");

		let leap_year = year % 400 == 0 || (year % 4 == 0 && year % 100 != 0);
		match *self {
			Month::February => if leap_year { 29 } else { 28 },
			Month::April | Month::June | Month::September | Month::November => 30,
			_ => 31
		}
	}

	pub fn next_month(&self) -> Self
	{
		match* self {
			Month::January   => Month::February,
			Month::February  => Month::March,
			Month::March     => Month::April,
			Month::April     => Month::May,
			Month::May       => Month::June,
			Month::June      => Month::July,
			Month::July      => Month::August,
			Month::August    => Month::September,
			Month::September => Month::October,
			Month::October   => Month::November,
			Month::November  => Month::December,
			Month::December  => Month::January
		}
	}

	pub fn prev_month(&self) -> Self
	{
		match *self {
			Month::January   => Month::February,
			Month::February  => Month::March,
			Month::March     => Month::April,
			Month::April     => Month::May,
			Month::May       => Month::June,
			Month::June      => Month::July,
			Month::July      => Month::August,
			Month::August    => Month::September,
			Month::September => Month::October,
			Month::October   => Month::November,
			Month::November  => Month::December,
			Month::December  => Month::January
		}
	}
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum DayOfWeek
{
	Sunday,
	Monday,
	Tuesday,
	Wednesday,
	Thursday,
	Friday,
	Saturday
}

#[cfg(test)]
mod test
{
	use super::*;

	#[test]
	fn day_of_week()
	{
		assert_eq!(Date::new(1900, Month::January, 1).day_of_week(), DayOfWeek::Monday);
		assert_eq!(Date::new(2018, Month::January, 9).day_of_week(), DayOfWeek::Tuesday);
		assert_eq!(Date::new(2017, Month::November, 23).day_of_week(), DayOfWeek::Thursday);
	}
}
