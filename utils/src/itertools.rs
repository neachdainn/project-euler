/// Types that can be can be converted into a double-ended iterator.
pub trait IntoDoubleEndedIterator: IntoIterator
{
	type IntoRevIter: DoubleEndedIterator<Item=Self::Item>;

	fn into_double_ended_iter(self) -> Self::IntoRevIter;
}

impl<I> IntoDoubleEndedIterator for I
	where I: IntoIterator,
	      I::IntoIter: DoubleEndedIterator<Item=I::Item>
{
	type IntoRevIter = I::IntoIter;

	fn into_double_ended_iter(self) -> Self::IntoRevIter
	{
		self.into_iter()
	}
}

/// Iterates over a view of the underlying slice.
///
/// The difference between this and the standard library's `Windowed` is that
/// this allows for incomplete windows.
pub struct Convolution<'a, T>
	where T: 'a
{
	start: usize,
	stop: usize,
	len: usize,
	inner: &'a [T]
}
impl<'a, T> Convolution<'a, T>
{
	pub fn new(inner: &'a [T], len: usize) -> Self
	{
		Convolution { start: 0, stop: 0, len, inner }
	}
}
impl<'a, T> Iterator for Convolution<'a, T>
{
	type Item = &'a [T];

	fn next(&mut self) -> Option<Self::Item>
	{
		self.start += if self.stop - self.start == self.len { 1 } else { 0 };
		self.stop += 1;

		if self.start >= self.inner.len() {
			None
		} else {
			let end = ::std::cmp::min(self.stop, self.inner.len());
			Some(&self.inner[self.start..end])
		}
	}
}

#[cfg(test)]
mod test
{
	#[test]
	fn windowed()
	{
		let mut iter = super::Convolution::new(&[1, 2, 3], 2);
		assert_eq!(iter.next().unwrap(), &[1]);
		assert_eq!(iter.next().unwrap(), &[1, 2]);
		assert_eq!(iter.next().unwrap(), &[2, 3]);
		assert_eq!(iter.next().unwrap(), &[3]);
		assert_eq!(iter.next(), None);
	}

	#[test]
	fn windowed_too_large()
	{
		let mut iter = super::Convolution::new(&[1], 3);
		assert_eq!(iter.next().unwrap(), &[1]);
		assert_eq!(iter.next().unwrap(), &[1]);
		assert_eq!(iter.next().unwrap(), &[1]);
		assert_eq!(iter.next(), None);
	}
}
