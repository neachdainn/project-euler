/// Find the value of `d < 1000` for which d⁻¹ contains the longest recurring
/// cycle in its decimal fraction part.
pub fn solution() -> usize
{
	(1..1000).rev().map(|e| (e, sequence_length(e))).scan((0, 0), |max_seen, (e, s)| {
		if e < max_seen.1 { return None; }

		if max_seen.1 < s { *max_seen = (e, s); }
		Some(*max_seen)
	}).map(|(e, _)| e).last().unwrap()
}

fn sequence_length(d: usize) -> usize
{
	let mut seen = vec![0; d];

	let mut pos = 0;
	let mut val = 1;

	while val > 0 && seen[val] == 0 {
		seen[val] = pos;

		val = (val * 10) % d;
		pos += 1;
	}

	pos - seen[val]
}

#[cfg(test)]
mod test
{
	#[test]
	fn project_euler_026()
	{
		assert_eq!(super::solution(), 983, "Incorrect solution");
	}
}
