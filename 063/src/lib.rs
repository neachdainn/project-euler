//! Powerful digit counts
//!
//! # Problem Description.
//!
//! The 5-digit number, 16807=7⁵, is also a fifth power. Similarly, the 9-digit
//! number, 134217728=8⁹, is a ninth power.
//!
//! How many n-digit positive integers exist which are also an <i>n</i>th
//! power?


/// The type of the answer.
type AnswerType = usize;

/// The answer to the problem.
pub const ANSWER: AnswerType = 49;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_063::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	// This problem sounds a lot more difficult than it really is. 10^x will
	// always have more than x digits, so we only need to worry about single
	// digit numbers. As such, we just need to find the point where 9^x has
	// less than x digits; the point where 9^x < 10^(x - 1) ⇒ x = 22.
	//
	// Unfortunately, that doesn't fit into a `u64`. Good thing we don't need
	// to be 100% accurate.
	(1..23).map(|x| {
		(1..10).map(|n| ((n as f64).powi(x).log10() + 1.0) as i32)
		          .filter(|&n| n == x)
		          .count()
	}).sum()
}
