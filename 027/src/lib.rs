extern crate utils;
use utils::primes::SieveOfEratosthenes;

/// The upper limit of the search space.
const LIMIT: i64 = 1000;

/// Find the product of the coefficients, `a` and `b`, for the quadratic
/// expression that produces the maximum number of primes for consecutive values
/// of `n`, starting with `n = 0`.
pub fn solution() -> i64
{
	// Just a guess at the largest value of `n`.
	let max = expression(LIMIT, LIMIT, LIMIT);

	// Compute the primes
	let sieve = SieveOfEratosthenes::new(max as usize);

	// Function to count consecutive primes using the expression.
	let consecutive_primes = |a, b| {
		(0..).map(|n| expression(a, b, n))
		     .take_while(|&v| v > 0 && sieve.is_prime(v as usize))
		     .count()
	};

	// Now do the search across [-LIMIT, LIMIT]x[-LIMIT, LIMIT]
	(-LIMIT..LIMIT+1).flat_map(|a| (-LIMIT..LIMIT+1).map(move |b| (a, b)))
	                 .map(|(a, b)| (a, b, consecutive_primes(a, b)))
	                 .max_by_key(|&(_, _, p)| p)
	                 .map(|(a, b, _)| a * b)
	                 .unwrap()
}

fn expression(a: i64, b: i64, n: i64) -> i64
{
	n * n + a * n + b
}


#[cfg(test)]
mod test
{
	#[test]
	fn project_euler_027()
	{
		assert_eq!(super::solution(), -59231, "Incorrect solution");
	}
}
