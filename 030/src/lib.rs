/// The upper limit of our search space.
///
/// There is a point where the sum of the fifth power of the digits has less
/// digits than the number. At this point, the summing just can't keep pace
/// with the increased number of digits.
///
/// `6 * 9^5 = 354,294` and `7 * 9^5 = 413,343`. This means that our upper
/// limit is 354,295 as no seven (or more) digit number could possibly be the
/// sum of the fifth power of their digits.
const LIMIT: usize = 354_294 + 1;

/// Find the sum of all the numbers that can be written as the sum of fifth
/// powers of their digits.
pub fn solution() -> usize
{
	// We skip one (and zero), as per the problem directions
	(2..LIMIT).filter(|&n| n == sum_of_fifth_power(n)).sum()
}

/// Return the sum of the fifth power of all the digits.
///
/// This will panic if the input number is larger than the problem limit.
fn sum_of_fifth_power(num: usize) -> usize
{
	// It turns out, trying to do this with loops an iterators is more
	// complicated than typing this all out and, if I had to guess, would
	// compile to less efficient instructions.
	assert!(num < LIMIT);

	((num / 100_000) % 10).pow(5) +
	((num /  10_000) % 10).pow(5) +
	((num /   1_000) % 10).pow(5) +
	((num /     100) % 10).pow(5) +
	((num /      10) % 10).pow(5) +
	((num /       1) % 10).pow(5)
}

#[cfg(test)]
mod test
{
	#[test]
	fn project_euler_030()
	{
		assert_eq!(super::solution(), 443839, "Incorrect solution");
	}
}
