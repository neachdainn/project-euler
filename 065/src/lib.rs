//! Convergents of e
//!
//! # Problem Description.
//!
//! The square root of 2 can be written as an infinite continued fraction.
//!
//! The infinite continued fraction can be written, √2 = [1;(2)], (2) indicates
//! that 2 repeats ad infinitum. In a similar way, √23 = [4;(1,3,1,8)].
//!
//! It turns out that the sequence of partial values of continued fractions for
//! square roots provide the best rational approximations. Let us consider the
//! convergents for √2: 1, 3/2, 7/5, 17/12, 41/29, 99/70, 239/169, 577/408,
//! 1393/985, 3363/2378, …
//!
//! What is most surprising is that the important mathematical constant, e =
//! [2; 1,2,1, 1,4,1, 1,6,1 , … , 1,2k,1, …].
//!
//! The first ten terms in the sequence of convergents for e are: 2, 3, 8/3,
//! 11/4, 19/7, 87/32, 106/39, 193/71, 1264/465, 1457/536, …
//!
//! The sum of digits in the numerator of the 10th convergent is 1+4+5+7=17.
//!
//! Find the sum of digits in the numerator of the 100th convergent of the
//! continued fraction for e.

extern crate utils;
use utils::num::{BigUInt, Digits};

/// The type of the answer.
type AnswerType = u32;

/// The answer to the problem.
pub const ANSWER: AnswerType = 272;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_065::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	let e = (0..).scan(0u32, |acc, i| if i % 3 != 1 { Some(1) } else { *acc += 2; Some(*acc) });

	// In a generalized continued fraction, X_n = A_n / B_n where
	// A_n = b_n * A_{n-1} + a_n * A_{n-2}
	// with A_{-1} = 1 and A_0 = a_0
	let numerator = e
		.take(99)
		.fold((BigUInt::new(1), BigUInt::new(2)), |mut acc, a| {
			acc.0 += acc.1.clone() * a;
			(acc.1, acc.0)
		}).1;
	numerator.into_digits(10).sum()
}
