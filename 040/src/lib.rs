extern crate utils;

use utils::num::Digits;

/// Solve Problem 40
///
/// The question would take way too much special formatting to correctly place
/// in this documentation.
pub fn solution() -> u32
{
	let mut iter = (1..).flat_map(|n| n.into_digits(10).rev());

	let d = [iter.nth((1 - 0)            - 1).unwrap(),
	         iter.nth((10 - 1)           - 1).unwrap(),
	         iter.nth((100 - 10)         - 1).unwrap(),
	         iter.nth((1000 - 100)       - 1).unwrap(),
	         iter.nth((10000 - 1000)     - 1).unwrap(),
	         iter.nth((100000 - 10000)   - 1).unwrap(),
	         iter.nth((1000000 - 100000) - 1).unwrap()];

	d.iter().product()
}

#[cfg(test)]
mod test
{
	#[test]
	fn project_euler_040()
	{
		assert_eq!(super::solution(), 210, "Incorrect solution");
	}
}
