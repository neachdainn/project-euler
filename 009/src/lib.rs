//! Special Pythagorean triplet
//!
//! # Problem Description.
//!
//! A Pythagorean triplet is a set of three natural numbers, *a* < *b* < *c*,
//! for which, <i>a</i>² + <i>b</i>² = <i>c</i>².
//!
//! For example, 3² + 4² = 9 + 16 = 25 = 5².
//!
//! There exists exactly one Pythagorean triplet for which *a* + *b* + *c* =
//! 1000.
//!
//! Find the product *abc*.


extern crate utils;
use utils::PythagoreanTriples;

/// The type of the answer.
type AnswerType = u32;

/// The answer to the problem.
pub const ANSWER: AnswerType = 31875000;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_009::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	// This iterates over the primitives and (a, b, c) being a triple means
	// that (na, nb, nc) is a triple for any integer n. So we just need to find
	// a triple that divides 1000.
	PythagoreanTriples::<u32>::new().find(|&(a, b, c)| 1000 % (a + b + c) == 0)
	                         .map(|(a, b, c)|  { let n = 1000 / (a + b + c); (n * a, n * b, n * c) })
	                         .map(|(a, b, c)| a * b * c).unwrap()
}
