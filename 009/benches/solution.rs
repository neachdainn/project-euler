#![feature(test)]

extern crate euler_009;
extern crate test;
use test::{Bencher, black_box};

#[bench]
fn solution(b: &mut Bencher)
{
	b.iter(|| black_box(euler_009::solution()))
}
