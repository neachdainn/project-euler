//! Amicable numbers
//!
//! # Problem Description.
//!
//! Let d(n) be defined as the sum of proper divisors of n (numbers less than n
//! which divide evenly into n). If d(a) = b and d(b) = a, where a ≠ b, then a
//! and b are an amicable pair and each of a and b are called amicable numbers.
//!
//! For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44,
//! 55 and 110; therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4,
//! 71 and 142; so d(284) = 220.
//!
//! Evaluate the sum of all the amicable numbers under 10000.

extern crate utils;
use utils::{mem::Memoizer, primes::Sieve};

/// The type of the answer.
type AnswerType = u32;

/// The answer to the problem.
pub const ANSWER: AnswerType = 31626;

/// Find the solution to the problem.
///
/// The return value of this function should equal `ANSWER`. The run time of
/// the function should be less than one second on release builds.
///
/// ```
/// # use euler_021::{ANSWER, solution};
/// assert_eq!(solution(), ANSWER, "Incorrect solution");
/// ```
pub fn solution() -> AnswerType
{
	let sieve = Sieve::new(100);
	let memoizer = Memoizer::new(move |_, num| {
		// This isn't very Rusty, but it works and was much easier to reason
		// about. Basically, we are implementing the Sum of Divisors function.
		let mut n = num;
		let mut sum = 1;
		for p in sieve.iter() {
			if p * p > n || n == 1 {
				break;
			}

			if n% p == 0 {
				let mut j = p * p;
				n /= p;

				while n % p == 0 {
					j *= p;
					n /= p;
				}
				sum = (sum * (j - 1)) / (p - 1);
			}
		}

		if n > 1 {
			sum *= n + 1;
		}

		sum - num
	});

	(2..10000).filter(|&d| {
		let v = memoizer.call(d);
		v != d && d == memoizer.call(v)
	}).sum()
}
